from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
class HmmerFixerDatabase(models.Model):
    main_file = models.CharField(blank=True, max_length = 200)
    skipped_seqs = models.CharField(blank=True, max_length = 200)
    user_id = models.IntegerField()
    anal_name = models.CharField(blank=True, max_length = 200)
    link = models.CharField(max_length = 200)
    is_favourite = models.BooleanField(default=False)
    created_at = models.DateTimeField(
        _("Created"), blank=True, auto_now_add=True,null=True)
    def __str__(self):
        return self.anal_name