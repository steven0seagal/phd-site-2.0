from django.db import models
from django.utils.translation import gettext_lazy as _


class PeekUserData(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)

    def __str__(self):
        return self.username


class CompleteQueue(models.Model):
    user_id = models.IntegerField()
    tool = models.CharField(max_length=10)
    status = models.CharField(max_length=25)
    analysis_name = models.CharField(max_length=100)
    script = models.CharField(max_length=1000000)
    file = models.CharField(max_length=200, blank=True)
    is_favourite = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(_("Created"), blank=True, auto_now_add=True, null=True)
    uploaded = models.BooleanField(default=False)
    ontology_file = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.analysis_name


class QueForFfas(models.Model):
    user_id = models.IntegerField()
    tool = models.CharField(max_length=10)
    status = models.CharField(max_length=25)
    analysis_name = models.CharField(max_length=100)
    script = models.CharField(max_length=2000)
    is_favourite = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(_("Created"), blank=True, auto_now_add=True, null=True)
    uploaded = models.BooleanField(default=False)
    downloaded = models.BooleanField(default=False)

    def __str__(self):
        return self.analysis_name


class QueForPCH(models.Model):
    user_id = models.IntegerField()
    tool = models.CharField(max_length=10)
    status = models.CharField(max_length=25)
    analysis_name = models.CharField(max_length=100)
    script = models.CharField(max_length=2000)
    is_favourite = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(_("Created"), blank=True, auto_now_add=True, null=True)
    uploaded = models.BooleanField(default=False)

    def __str__(self):
        return self.analysis_name
