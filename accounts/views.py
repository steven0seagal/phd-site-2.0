from datetime import datetime

from django.contrib import messages, auth
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django_tables2 import RequestConfig

from alignments_tools.models import ColapserDatabase
from hmmer_fixer.models import HmmerFixerDatabase
from neighborhood_analysis.models import NeighAnalyzSequenceDatabase,NeighAnalyzDatabase
from scripts import check_link_existance
from scripts.data_from_script import feedMe
from .models import CompleteQueue, QueForFfas, QueForPCH
from .tables import NeighAnalyzSequenceTable, NeighAnalyzTable


def register(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form_data = request.POST
        first_name, last_name = form_data['first_name'], form_data['last_name']
        username, email = form_data['username'], form_data['email']
        password, password2 = form_data['password'], form_data['password2']

        if password == password2:
            if User.objects.filter(username=username).exists():
                messages.error(request, 'That username is taken')
            elif User.objects.filter(email=email).exists():
                messages.error(request, 'That email is being used')
            else:
                User.objects.create_user(username=username, email=email, password=password,
                                         first_name=first_name, last_name=last_name).save()
                messages.success(request, 'You are now registered and can log in')
                return redirect('login')
        else:
            messages.error(request, 'Passwords do not match')
        return redirect('register')
    return render(request, 'user_panel/register.html')


def login(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        username, password = request.POST['username'], request.POST['password']
        user = auth.authenticate(username=username, password=password)

        if user:
            auth.login(request, user)
            messages.success(request, 'You are now logged in')
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid credentials')
        return redirect('login')
    return render(request, 'user_panel/login.html')


def logout(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        auth.logout(request)
        messages.success(request, 'You are now logged out')
        return redirect('login')


def extract_neigh_seq_data(database_objects: list) -> list:
    return [
        {
            'id': job.id,
            'sequence_title': job.sequence_title,
            'blastp_cutoff': job.blast_cutoff,
            'range': job.neigh_size,
            'cutoff': job.cut_off,
            'strand': job.strand,
            'correction': job.test_correction,
            'analysis_name': job.analysis_name,
            'status': job.status,
            'link': job.output_results.split('/')[-1].split('.')[0],
            'ontology': job.ontology_results,
            'created_at': job.created_time,
            'is_favourite': job.is_favorite,
            'query': job.sequence,
            'taxonomy_file': job.output_results.replace('.txt', '_taxonomy.txt')
        }
        for job in database_objects
    ]


def dashboard_view(request: HttpRequest, job_type, template: str, context_key: str) -> HttpResponse:
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    all_jobs = job_type.objects.order_by('-id').filter(user_id=request.user.id)
    jobs = feedMe(all_jobs, context_key) if context_key else extract_neigh_seq_data(all_jobs)
    context = {'current_time': current_time, f'{context_key}_jobs': jobs}
    return render(request, template, context)


def dashboard_neigh(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    all_jobs = CompleteQueue.objects.order_by('-id').filter(user_id=request.user.id)
    neigh_domain_jobs = feedMe(all_jobs, 'NA')
    neigh_gene_jobs = feedMe(all_jobs, 'NAG')
    neigh_domain_family_jobs = feedMe(all_jobs, "NAF")
    neigh_domain_jobs_all = feedMe(all_jobs, "NAD")
    neigh_jobs = neigh_domain_jobs + neigh_gene_jobs + neigh_domain_family_jobs + neigh_domain_jobs_all
    neigh_jobs_sorted = sorted(neigh_jobs, key=lambda k: k['id'], reverse=True)

    context = {
        'current_time': current_time,
        'neigh_example_jobs': neigh_jobs_sorted,
    }
    return render(request, 'user_panel/dashboard_neigh.html', context)


def dashboard_neigh_seq(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    all_jobs = NeighAnalyzSequenceDatabase.objects.filter(user_id=request.user.id).order_by('-id')
    favourite_jobs = [job for job in all_jobs if job.is_favorite]
    other_jobs = [job for job in all_jobs if not job.is_favorite]

    table_favorite = NeighAnalyzSequenceTable(favourite_jobs)
    RequestConfig(request).configure(table_favorite)

    table_other = NeighAnalyzSequenceTable(other_jobs)
    RequestConfig(request).configure(table_other)

    context = {
        'current_time': current_time,
        'table_favorite': table_favorite,
        'table_other': table_other,
    }
    return render(request, 'user_panel/dashboard_neigh_seq.html', context)



def dashboard_ffas(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    ffas_database_jobs = QueForFfas.objects.order_by('-id').filter(user_id=request.user.id)
    ffas_jobs = feedMe(ffas_database_jobs, 'FFAS')

    context = {
        'current_time': current_time,
        'ffas_jobs': ffas_jobs,
    }
    return render(request, 'user_panel/dashboard_ffas.html', context)


def dashboard_stretch(request):
    all_jobs = CompleteQueue.objects.order_by('-id').filter(user_id=request.user.id)

    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    stretch_example_jobs = feedMe(all_jobs, 'M3A')

    context = {
        'current_time': current_time,
        'stretch_example_jobs': stretch_example_jobs,

    }
    return render(request, 'user_panel/dashboard_stretch.html', context)


def dashboard_collaps(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    colapser_jobs = ColapserDatabase.objects.order_by('-id').filter(user_id=request.user.id)
    checked_data_colapser = check_link_existance.link_ready(colapser_jobs)

    context = {
        'current_time': current_time,
        'checked_data_colapser': checked_data_colapser,
    }

    return render(request, 'user_panel/dashboard_colaps.html', context)


def dashboard_hmmer(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    hmmer_jobs = HmmerFixerDatabase.objects.order_by('-id').filter(user_id=request.user.id)
    checked_hmmer_jobs = check_link_existance.link_ready(hmmer_jobs)

    context = {
        'current_time': current_time,
        'checked_hmmer_jobs': checked_hmmer_jobs,
    }
    return render(request, 'user_panel/dashboard_hmmer.html', context)


def dashboard_pch(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    pch_database_jobs = QueForPCH.objects.order_by('-id').filter(user_id=request.user.id)
    pch_jobs = feedMe(pch_database_jobs, 'PCH')

    context = {
        'current_time': current_time,
        'pch_jobs': pch_jobs,
    }

    return render(request, 'user_panel/dashboard_pch.html', context)

def dashboard(request: HttpRequest) -> HttpResponse:
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    user_id = request.user.id

    context = {
        'current_time': current_time,
        'stretch_example_jobs': feedMe(CompleteQueue.objects.filter(user_id=user_id), 'M3A'),
        'checked_data_colapser': check_link_existance.link_ready(ColapserDatabase.objects.filter(user_id=user_id)),
        'checked_hmmer_jobs': check_link_existance.link_ready(HmmerFixerDatabase.objects.filter(user_id=user_id)),
        'neigh_example_jobs': sorted(
            sum([feedMe(CompleteQueue.objects.filter(user_id=user_id), job_type)
                 for job_type in ['NA', 'NAG', 'NAF', 'NAD']], []),
            key=lambda k: k['id'], reverse=True),
        'ffas_jobs': feedMe(QueForFfas.objects.filter(user_id=user_id), 'FFAS'),
        'pch_jobs': feedMe(QueForPCH.objects.filter(user_id=user_id), 'PCH'),
    }
    return render(request, 'user_panel/dashboard.html', context)


def toggle_favourite_task(request: HttpRequest, model, job_id: int, redirect_view: str) -> HttpResponse:
    job = model.objects.get(id=job_id)
    job.is_favorite = not job.is_favorite
    job.save()
    return redirect(redirect_view)


def make_favourite_task(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, CompleteQueue, job_id, 'dash_neigh')


def make_favourite_task_ffas(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, QueForFfas, job_id, 'dash_ffas')


def make_favourite_task_pch(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, QueForPCH, job_id, 'dash_human')


def make_favourite_task_colaps(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, ColapserDatabase, job_id, 'dash_collaps')


def make_favourite_task_stretch(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, CompleteQueue, job_id, 'dash_stretch')


def make_favourite_task_hmmer(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, HmmerFixerDatabase, job_id, 'dash_hmmer')


def make_favourite_task_seq(request: HttpRequest, job_id: int) -> HttpResponse:
    return toggle_favourite_task(request, NeighAnalyzSequenceDatabase, job_id, 'dash_neigh_seq')


def view_sequence(request, pk):
    sequence_data = get_object_or_404(NeighAnalyzSequenceDatabase, pk=pk)
    return render(request, 'user_panel/view_sequence.html', {'sequence_data': sequence_data})
