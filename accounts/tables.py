import django_tables2 as tables
from neighborhood_analysis.models import NeighAnalyzSequenceDatabase
from django.utils.html import format_html
class NeighAnalyzSequenceTable(tables.Table):

    def render_sequence(self, record):
        return format_html('<a href="#" onclick="window.open(\'/accounts/sequence/{}/\', \'_blank\', \'width=600,height=400\');" class="btn btn-primary btn-sm">View Sequence</a>', record.id)

    def render_is_favorite(self, record):
        if record.is_favorite:
            star_color = 'orange'
        else:
            star_color = 'gray'
        return format_html(
            '<a href="/accounts/dashboard/neigh-seq/{}" class="btn btn-outline-secondary">'
            '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="{}" class="bi bi-star-fill" viewBox="0 0 16 16">'
            '<path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>'
            '</svg></a>',
            record.pk, star_color
        )

    def render_output_results(self, record):
        return format_html('<a href="{}" class="btn btn-primary btn-sm">Download</a>', record.output_results)

    def render_ontology_results(self, record):
        return format_html('<a href="{}" class="btn btn-primary btn-sm">Download</a>', record.ontology_results)
    class Meta:
        model = NeighAnalyzSequenceDatabase
        template_name = "django_tables2/bootstrap.html"
        fields = ("id", "sequence_title", "blast_cutoff", "neigh_size", "cut_off", "strand",
                  "test_correction", "analysis_name", "status", "output_results",
                  "ontology_results", "created_time", "is_favorite", "sequence")


class NeighAnalyzTable(tables.Table):
    def render_is_favorite(self, record):
        if record.is_favorite:
            star_color = 'orange'
        else:
            star_color = 'gray'
        return format_html(
            '<a href="/accounts/dashboard/neighborhood/{}" class="btn btn-outline-secondary">'
            '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="{}" class="bi bi-star-fill" viewBox="0 0 16 16">'
            '<path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>'
            '</svg></a>',
            record.pk, star_color
        )

    def render_output_results(self, record):
        return format_html('<a href="{}" class="btn btn-primary btn-sm">Download</a>', record.output_results)

    def render_ontology_results(self, record):
        return format_html('<a href="{}" class="btn btn-primary btn-sm">Download</a>', record.ontology_results)

    class Meta:
        model = NeighAnalyzSequenceDatabase
        template_name = "django_tables2/bootstrap.html"
        fields = ("id", "sequence_title", "blast_cutoff", "neigh_size", "cut_off", "strand",
                  "test_correction", "analysis_name", "status", "output_results",
                  "ontology_results", "created_time", "is_favorite", "sequence")