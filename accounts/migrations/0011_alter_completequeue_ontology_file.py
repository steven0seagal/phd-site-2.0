# Generated by Django 3.2.6 on 2022-11-22 10:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_completequeue_ontology_file'),
    ]

    operations = [
        migrations.AlterField(
            model_name='completequeue',
            name='ontology_file',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
