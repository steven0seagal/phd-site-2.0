# Generated by Django 2.1.7 on 2020-05-25 10:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20200420_1945'),
    ]

    operations = [
        migrations.CreateModel(
            name='QueForFfas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('tool', models.CharField(max_length=10)),
                ('status', models.CharField(max_length=25)),
                ('analysis_name', models.CharField(max_length=100)),
                ('script', models.CharField(max_length=2000)),
            ],
        ),
    ]
