from django.urls import path

from . import views

urlpatterns = [
    path('login', views.login, name='login'),
    # path('register', views.register, name = 'register'),
    path('logout', views.logout, name='logout'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('dashboard/neighborhood', views.dashboard_neigh, name='dash_neigh'),
    path('dashboard/ffas', views.dashboard_ffas, name='dash_ffas'),
    path('dashboard/colapser', views.dashboard_collaps, name='dash_collaps'),
    path('dashboard/stretcher', views.dashboard_stretch, name='dash_stretch'),
    path('dashboard/hhmer', views.dashboard_hmmer, name='dash_hmmer'),
    path('dashboard/human-prot', views.dashboard_pch, name='dash_human'),
    path('dashboard/neigh-seq', views.dashboard_neigh_seq, name='dash_neigh_seq'),

    path('dashboard/neighborhood/<job_id>', views.make_favourite_task),
    path('dashboard/ffas/<job_id>', views.make_favourite_task_ffas),
    path('dashboard/colapser/<job_id>', views.make_favourite_task_colaps),
    path('dashboard/stretcher/<job_id>', views.make_favourite_task_stretch),
    path('dashboard/hmmer/<job_id>', views.make_favourite_task_hmmer),
    path('dashboard/human-prot/<job_id>', views.make_favourite_task_pch),
    path('dashboard/neigh-seq/<job_id>', views.make_favourite_task_seq),
    path('sequence/<int:pk>/', views.view_sequence, name='view_sequence'),

]
