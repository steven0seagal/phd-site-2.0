from django.urls import path
from . import views

urlpatterns = [
    path('',views.ffas_main, name = 'ffas_main'),
    path('ffas_analysis', views.ffas_analysis, name='ffas_analysis'),
    path('api/get_ffas_analysis/', views.GetFFASAnalysis.as_view(), name="ffas_analysis"),
    path('api/send_ffas_analysis/', views.UploadFFASResults.as_view(), name="send_ffas_analysis"),
    path('api/update_ffas_analysis/', views.UpdateFFASStatus.as_view(), name="update_ffas_analysis"),

]