import random
import string

from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.utils.datastructures import MultiValueDictKeyError

from accounts.models import QueForFfas
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


def ffas_main(request):
    title = "FFAS03"
    return render(request, 'tools/input/ffas_start.html')


def ffas_analysis(request):
    out_name = request.POST['out_name']
    user_id = request.user.id

    context = {}
    uploaded_file = request.FILES['input']
    fs = FileSystemStorage()
    name = fs.save(uploaded_file.name, uploaded_file)
    context['url'] = fs.url(name)
    server_results_file = "/media/"
    server_filestore = "/media/results/"
    link_do_pliku = context['url']

    letters = string.ascii_lowercase
    end_end = ''.join(random.choice(letters) for i in range(10))
    link_down = '/media/results/' + end_end + '.ff'
    db_initial = 'profile'

    ready_script = 'python3 /usr/src/app/scripts/execute_order_101.py ' \
                   '{0} {1} {2} '.format(db_initial, link_do_pliku, link_down)

    boxes = ['PDB', 'SCOP', 'PFAM', 'Hsapiens', 'COG', 'VFDB', 'VFdbcustom']
    databases = ['profile']
    for box in boxes:
        try:
            request.POST[box + "_box"]
            databases.append(box)
        except MultiValueDictKeyError:
            continue
    if len(databases) > 1:
        for db in databases[1:]:
            new_file_url = link_down
            new_result_file = '/media/results/' + end_end + "_" + db.lower() + ".fft"
            ready_script += " && python3 /usr/src/app" \
                            "/scripts/execute_order_101.py {0} {1} {2} ".format(db, new_file_url, new_result_file)

    job = QueForFfas(user_id=user_id, status='Queue', analysis_name=out_name, script=ready_script, tool="FFAS", )
    job.save()

    return redirect('dashboard')

class GetFFASAnalysis(APIView):
    """
    Get FFAS analysis
    """
    def get(self, request):
        analysis = QueForFfas.object.filter(status='Queue').first()
        if analysis:
            analysis.status = 'Running'
            analysis.save()
            return Response({'script': analysis.script, 'analysis_id': analysis.id}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'No analysis in queue'}, status=status.HTTP_404_NOT_FOUND)

class UploadFFASResults(APIView):
    """
    Upload FFAS results
    """
    def post(self, request):
        analysis_id = request.data['analysis_id']
        analysis = QueForFfas.object.get(id=analysis_id)
        analysis.status = 'Done'
        analysis.save()
        fileobjects = request.FILES.getlist('fileobjects')
        if not fileobjects:
            return Response({'message': 'No files uploaded'}, status=status.HTTP_400_BAD_REQUEST)
        for fileobject in fileobjects:
            with open('/app/media/results/' + fileobject.name, 'wb') as f:
                f.write(fileobject.read())
        return Response({'message': 'Files uploaded successfully'}, status=status.HTTP_200_OK)

class UpdateFFASStatus(APIView):
    """
    Update FFAS results
    """
    def post(self, request):
        analysis_id = request.data['analysis_id']
        status = request.data['status']
        analysis = QueForFfas.object.get(id=analysis_id)
        analysis.status = status
        analysis.save()
        return Response({'message': 'Analysis status updated successfully'}, status=status.HTTP_200_OK)

