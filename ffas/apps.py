from django.apps import AppConfig


class FfasConfig(AppConfig):
    name = 'ffas'
