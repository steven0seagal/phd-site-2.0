from django.db import models


class Family(models.Model):

    family_id = models.CharField(max_length=255, db_index=True)
    family_name = models.CharField(max_length=255, db_index=True)
    description = models.TextField(null=True, blank=True)
    certificate = models.TextField(blank=True, null=True)
    public = models.BooleanField(default=False)

    def __str__(self):
        return self.family_name


class MatchingSeq(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class ExternalDB(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    source = models.CharField(max_length=255)
    site = models.CharField(max_length=255)
    db_id = models.CharField(max_length=255, db_index=True)

    def __str__(self):
        return self.site

class HMMmodels(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    hmm_family_id = models.CharField(max_length=255)
    hmm_file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name
        
class FullSeq(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class DomainSeq(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class Tasks(models.Model):

    user_id = models.IntegerField(blank=True)
    sequence_title = models.CharField(max_length=255, blank=True)
    sequence = models.TextField()
    status = models.CharField(max_length=255,default='Pending')
    created_time = models.DateTimeField(auto_now_add=True)
    uniqe_id = models.CharField(max_length=255,blank=True)
    uploaded = models.BooleanField(default=False)
    def __str__(self):
        return self.uniqe_id

class ResultsHmm(models.Model):

    file_name = models.CharField(max_length=255)
    hmm_name = models.CharField(max_length=255)
    family_id = models.CharField(max_length=255, blank=True, null=True)
    family_name_in_results = models.CharField(max_length=255,blank=True,null=True)
    pfam_in_results = models.CharField(max_length=255,blank=True,null=True)
    link_to_family_site = models.CharField(max_length=255, blank=True,null=True)

    def __str__(self):
        return self.hmm_name

class WebLogo(models.Model):
    # image = models.ImageField(upload_to='photos/%Y/%m/%d/')
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    file = models.CharField(max_length=255)

class FoldedModel(models.Model):
    
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    file = models.CharField(max_length=255, blank=True,null=True)
    plddt = models.CharField(max_length=255, blank=True, null=True)

class Structure(models.Model):

    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    file = models.CharField(max_length=255, blank=True,null=True)

class PklDomain(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, blank=True,null=True)
    file = models.CharField(max_length=255, blank=True,null=True)