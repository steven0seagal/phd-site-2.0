from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
# from .models import Family, Clade, InterPro, Structure, Taxonomy, GeneOntology, ExternalDB, KeywordFamily, Citation, \
    # Tasks, WebLogo, ResultsHmm, MatchingSeq,FullSeq, DomainSeq
from .models import Family,MatchingSeq,FullSeq,DomainSeq, ResultsHmm, Tasks,WebLogo, ExternalDB, Structure,FoldedModel, PklDomain, HMMmodels
from django.db.models import Q
import uuid
from django.core.exceptions import ObjectDoesNotExist
# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

def kintaro_main(request):
    return(render(request,'kintaro/main.html'))

def result_search(request):
    context = {}
    query = request.GET.get('q')
    if query == None:
        context['family'] = None
    else:
        # FAMILY
        family_id_result = Family.objects.filter(Q(family_id__icontains=query))
        if len(family_id_result) > 0:
            context['family_id'] = []
            for i in family_id_result:
                context['family_id'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_id_count'] = len(family_id_result)

        # FAMILY NAME
        family_name_result = Family.objects.filter(Q(family_name__icontains=query))
        if len(family_name_result) > 0:
            context['family_name'] = []
            for i in family_name_result:
                context['family_name'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_name_count'] = len(family_name_result)

        # FAMILY DESCRIPTION
        family_description = []
        family_description_result = Family.objects.filter(Q(description__icontains=query))
        if len(family_description_result) > 0:
            context['family_description'] = []
            for i in family_description_result:
                if i.family_name not in family_description:
                    family_description.append(i.family_name)
                    context['family_description'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_description_count'] = len(family_description)

        # FAMILY CERTIFICATE
        family_certificate = []
        family_certificate_result = Family.objects.filter(Q(certificate__icontains=query))
        if len(family_certificate_result) > 0:
            context['family_certificate'] = []
            for i in family_certificate_result:
                if i.family_name not in family_certificate:
                    family_certificate.append(i.family_name)
                    context['family_certificate'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_certificate_count'] = len(family_certificate)



    return (render(request, 'kintaro/results.html', context))



class ListOfFamilies(TemplateView):
    template_name = '../templates/kintaro/database.html'

    # model = Family

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['results'] = {}
        families = Family.objects.filter(public=True).order_by('family_name')
        for family in families:
            context['results'][family.family_id] = {'name':family.family_name}

        return context



class SingleResult(TemplateView):
    template_name = '../templates/kintaro/single_family.html'

    def get_context_data(self, family_id, **kwargs):

        context = super().get_context_data(**kwargs)

        context['family'] = {}
        # context['clade'] = {}
        # context['interpro'] = {}
        context['structures'] = []
        # context['taxonomy'] = []
        # context['geneontology'] = []
        context['external'] = []
        # context['keyword'] = []
        # context['citation'] = []
        context['weblogo'] = {}
        context['aligned'] = {}
        context['unaligned'] = {}
        context['full'] = {}
        context['models']= []
        context['pkl_domains'] = []
        # context['hmm'] = None
        # FAMILY
        family_data = Family.objects.get(family_id=family_id)
        context['family']['name'] = family_data.family_name
        context['family']['id'] = family_data.family_id
        context['family']['description'] = family_data.description
        context['family']['certificate'] = family_data.certificate
        context['public'] = family_data.public
        # CLADE
        # clade_data = family_data.clade.all()[0]
        # context['clade']['name'] = clade_data.clade_name
        # context['clade']['id'] = clade_data.clade_id

        # # INTERPRO
        # try:
        #     interpro_data = InterPro.objects.get(family=family_data)
        #     context['interpro']['id'] = interpro_data.inter_id
        #     context['interpro']['abstract'] = interpro_data.abstract
        # except ObjectDoesNotExist:
        #     context['interpro']['id'] = None
        #     context['interpro']['abstract'] = None
        # STRUCTURES
        try:
            structures_data = Structure.objects.filter(family=family_data)
            for i in structures_data:
                context['structures'].append({'file': i.file})
        except ObjectDoesNotExist:
            context['structures'].append({'file': None})
        try:
            models_data = FoldedModel.objects.filter(family=family_data)
            for i in models_data:
                context['models'].append({'file':i.file,'plddt':i.plddt})
        except ObjectDoesNotExist:
            context['models'].append({'file':i.file, 'plddt':i.plddt})
        
        try:
            pkl_domains_data = PklDomain.objects.filter(family=family_data)
            for i in pkl_domains_data:
                context['pkl_domains'].append({'file':i.file,'name':i.name})
        except ObjectDoesNotExist:
            context['pkl_domains'].append({'file':None, 'name':None})
        # # TAXONOMY
        # try:
        #     taxonnomy_data = Taxonomy.objects.filter(family=family_data)
        #     for i in taxonnomy_data:
        #         context['taxonomy'].append({'tax_id': i.tax_id, 'tax_level': i.tax_level, 'tax_name':i.tax_name})
        # except ObjectDoesNotExist:
        #     context['taxonomy'].append({'tax_id': None})


        # # Geneontology
        # try:
        #     geneontology_data = GeneOntology.objects.filter(family=family_data)
        #     for i in geneontology_data:
        #         context['geneontology'].append({'go_id': i.go_id, 'go_name': i.go_name, 'go_category': i.go_category})
        # except ObjectDoesNotExist:
        #     context['geneontology'].append({'go_id': None, 'go_name': None, 'go_category': None})

        # EXTERNAL
        try:
            external_data = ExternalDB.objects.filter(family=family_data)
            for i in external_data:
                context['external'].append({'external_source': i.source, 'external_site': i.site, 'external_id': i.db_id})
        except ObjectDoesNotExist:
            context['external'].append({'external_source': None, 'external_site': None, 'external_id': None})

        # # KEYWORD
        # try:
        #     keyword_data = KeywordFamily.objects.filter(family=family_data)
        #     for i in keyword_data:
        #         context['keyword'].append(i)
        # except ObjectDoesNotExist:
        #     context['keyword'].append(None)

        # # CITATION
        # try:
        #     citation_data =Citation.objects.filter(family=family_data)
        #     for i in citation_data:
        #         context['citation'].append({'pmid':i.pmid, 'title':i.title, 'author':i.author,'scimag':i.sci_mag})
        # except ObjectDoesNotExist:
        #     context['citation'].append({'pmid':None, 'title':None, 'author':None, 'sci_mag':None})

        # # WEBLOGO
        try:
            weblogo_data = WebLogo.objects.get(family= family_data)
            context['weblogo'] = {'image':weblogo_data.file}
        except ObjectDoesNotExist:
            # context['weblogo'] = None
            pass
        # Aligned

        try:
            aligned_data = MatchingSeq.objects.get(family=family_data)
            context['aligned'] = {'file':aligned_data.file}
        except ObjectDoesNotExist:
            # context['aligned'] = None
            pass

        # Domain
        try:
            domain_data = DomainSeq.objects.get(family=family_data)
            context['unaligned'] = {'file':domain_data.file}
        except ObjectDoesNotExist:
            context['unaligned'] = None
        # FULL_
        try:
            full_data = FullSeq.objects.get(family=family_data)
            context['full'] = {'file':full_data.file}
        except ObjectDoesNotExist:
            context['full'] = None

        #HMM
        try:
            hmm_data = HMMmodels.objects.get(family=family_data)
            context['hmm'] = {'file':hmm_data.hmm_file}
        except ObjectDoesNotExist:
            # context['hmm'] = None
            pass

        return context

def result_check(request):
    if 'job_id' in request.GET:
        
        if request.GET['job_id']:
            job = request.GET['job_id']
            print(job)
            return redirect('hmmresult/{}'.format(job))
    else:
        return render(request, 'kintaro/browse_result.html')

def query_sequence(request):
    sequence = request.GET['sequence']
    if sequence[0] == '>':
        sequence_title = sequence.split('\n')[0]
        sequence = sequence.split('\n')[1:]
        
        sequence = ''.join([line.strip() for line in sequence])
    else:
        sequence_title = '>None sequence description'
        sequence = ''.join([line.strip() for line in sequence])

    uniqe_id =uuid.uuid4()
    Tasks(sequence=sequence, user_id=1, uniqe_id=uniqe_id,sequence_title=sequence_title).save()
    return redirect('hmmresult/{}'.format(uniqe_id))

def search_hmm_result(hmm_name_in_file):

    result = ResultsHmm.objects.get(hmm_name= hmm_name_in_file)
    return result
def pares_hmm_result(job_id):
    with open(f'/app/mediafiles/results/{job_id}','r') as handler:
        data = [x.strip().split() for x in handler]
    no_header = [x for x in data if x[0][0] != '#']
    print(no_header)
    complete_result = []
    for line in no_header:
        
        database_obj = search_hmm_result(line[0])

        is_public = check_if_public(database_obj.family_id)
        
        if is_public is True:
            
            complete_result.append({'family_id':database_obj.family_id, 'score':line[4], 'family_name_in_results':database_obj.family_name_in_results, 'pfam':database_obj.pfam_in_results})
        # complete_result.append('\t E.value: '.join([line[0],line[4]]))
    return complete_result

def check_if_public(family_id):

    family = Family.objects.get(family_id=family_id)
    return family.public

class HmmResult(TemplateView):
    template_name = '../templates/kintaro/hmmresult.html'

    def get_context_data(self, result_id, **kwargs):

        context = {}
        context['result_id'] = result_id
        database_data = Tasks.objects.filter(uniqe_id=result_id)
        # print(database_data[0].sequence)
        if len(database_data) == 0:
            context['status'] = 'NotFound'
            context['sequence_title'] = result.sequence_title
            context['query'] = database_data[0].sequence
            return context
        elif len(database_data) == 1:
            result = database_data[0]
            if result.status == 'Pending':
                context['status'] = 'Pending'
                context['sequence_title'] = result.sequence_title
                context['query'] = database_data[0].sequence
                return context
            elif result.status == 'Error':
                context['status'] = 'Error'
                context['sequence_title'] = result.sequence_title
                context['query'] = database_data[0].sequence
                return context
            elif result.status == 'Done' and result.uploaded is False:
                context['status'] = 'Pending'
                context['sequence_title'] = result.sequence_title
                context['query'] = database_data[0].sequence
                return context

            elif result.status == 'Done' and result.uploaded is True:
                context['status'] = 'Done'
                context['file'] = '/media/results/{}'.format(result_id)
                context['results'] = pares_hmm_result(result_id)
                context['query'] = database_data[0].sequence
                context['sequence_title'] = result.sequence_title


                return context

def searchsequence(request):
    return render(request, 'kintaro/search_seq.html')

class GetTaskToRun(APIView):

    def get(self, request):
        task = Tasks.objects.filter(status='Pending').first()
        if task:
            task.status = 'Running'
            task.save()
            return Response({'sequence': task.sequence, 'job_id': task.uniqe_id}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'No task to run'}, status=status.HTTP_404_NOT_FOUND)

class SaveResult(APIView):
    def post(self, request):
        """
        POST request to save the result of the task
        :param request:
        :return:
        """
        fileobjects = request.FILES.getlist('fileobjects')
        uniqe_id = request.data['unique_id']
        for file in fileobjects:
            with open(f'/app/mediafiles/results/{uniqe_id}','wb') as handler:
                for chunk in file.chunks():
                    handler.write(chunk)
        task = Tasks.objects.get(uniqe_id=uniqe_id)
        task.status = 'Done'
        task.uploaded = True
        task.save()
        return Response(status=status.HTTP_200_OK)

