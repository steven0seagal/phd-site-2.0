from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns=[
    path('',kintaro_main, name='kintaro_main'),
    path('families', ListOfFamilies.as_view(),name='kintaro_families'),
    path('family/<str:family_id>', SingleResult.as_view(), name='single_kintaro'),
    path('hmmresult/<str:result_id>', HmmResult.as_view(), name='hmm_result_kintaro'),
    path('hmmresult',result_check, name='browse_result_kintaro'),
    path('searchsequence', searchsequence, name='search_sequence_kintaro'),
    path('ass',query_sequence, name='query_sequence_kintaro'),
    path('results', result_search, name='kintaro_search_result'),
    path('api/get-task',GetTaskToRun.as_view(), name="get_task_kintaro"),
    path('api/upload-task', SaveResult.as_view(), name='upload_task_kintaro'),

]