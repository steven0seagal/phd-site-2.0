from django.contrib import admin
from .models import *
# Register your models here.

class FamilyAdmin(admin.ModelAdmin):
    list_display = ('family_id', 'family_name', 'public')
    list_display_links = ('family_id',)
    list_per_page = 25
    search_fields = ('family_name',)


admin.site.register(Family, FamilyAdmin)

class MatchingSeqAdmin(admin.ModelAdmin):
    list_display = ('family_name',)
    list_display_links = ('family_name',)
    list_per_page = ('family_name',)
    list_per_page = 25

admin.site.register(MatchingSeq, MatchingSeqAdmin)


class HMMModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'family_name', 'hmm_family_id')
    list_display_links = ('family_name',)
    list_per_page = 25
    search_fields = ('family_name','hmm_family_id')


admin.site.register(HMMmodels, HMMModelAdmin)



class ExternalDBAdmin(admin.ModelAdmin):
    list_display = ('source', 'site')
    list_display_links = ('source',)
    list_per_page = 25
    search_fields = ('source',)

admin.site.register(ExternalDB, ExternalDBAdmin)


class FullSeqAdmin(admin.ModelAdmin):
    list_display = ('family_name',)
    list_display_links = ('family_name',)
    list_per_page = ('family_name',)
    list_per_page = 25

admin.site.register(FullSeq, FullSeqAdmin)

class DomainSeqAdmin(admin.ModelAdmin):
    list_display = ('family_name',)
    list_display_links = ('family_name',)
    list_per_page = ('family_name',)
    list_per_page = 25

admin.site.register(DomainSeq, DomainSeqAdmin)

class ResultsHmmAdmin(admin.ModelAdmin):
    list_display = ('family_id',)
    list_display_links = ('family_id',)
    list_per_page = ('family_id',)
    list_per_page = 25

admin.site.register(ResultsHmm,ResultsHmmAdmin)

class TasksAdmin(admin.ModelAdmin):
    list_display = ('id','user_id','status','uniqe_id')
    list_display_links = ('id',)
    list_per_page = 25
    search_fields = ('user_id','status','uniqe_id')

admin.site.register(Tasks, TasksAdmin)

class WebLogoAdmin(admin.ModelAdmin):

    list_display = ('id', 'file')
    list_display_links = ('file',)
    list_per_page = 25
    search_fields = ('file',)

admin.site.register(WebLogo, WebLogoAdmin)

class FoldedModelAdmin(admin.ModelAdmin):

    list_display = ('id', 'file')
    list_display_links = ('file',)
    list_per_page = 25
    search_fields = ('file',)
admin.site.register(FoldedModel, FoldedModelAdmin)

class StructureAdmin(admin.ModelAdmin):
    list_display = ('id', 'file')
    list_display_links = ('file',)
    list_per_page = 25
    search_fields = ('file',)
admin.site.register(Structure,StructureAdmin)

class PklDomainAdmin(admin.ModelAdmin):
    list_display = ('id', 'file','name')
    list_display_links = ('file','name')
    list_per_page = 25
    search_fields = ('file','name')
admin.site.register(PklDomain,PklDomainAdmin)    