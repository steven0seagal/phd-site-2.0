# Generated by Django 3.2.6 on 2022-11-20 11:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kintaro', '0002_tasks'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResultsHmm',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_name', models.CharField(max_length=255)),
                ('hmm_name', models.CharField(max_length=255)),
                ('family_id', models.CharField(blank=True, max_length=255, null=True)),
                ('family_name_in_results', models.CharField(blank=True, max_length=255, null=True)),
                ('pfam_in_results', models.CharField(blank=True, max_length=255, null=True)),
                ('link_to_family_site', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
    ]
