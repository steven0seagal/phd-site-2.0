
from searchdb_coocurence.models import *

# #Escherichia
# print("Escherichia strains")
# with open("scripts_ready/searchdb_coocurence_colapsedonescherichiastrains_new.csv","r") as handler:
#     for line in handler:
#         ls = line.strip().split('\t')
#         ColapsedOnEscherichiaStrains(gene1=ls[1], gene2=ls[2], together=ls[3], first_only=ls[4], second_only=ls[5],
#                                      neither=ls[4], pvalue=ls[5],data_base='n').save()

# print("Escherichia species")
# with open("scripts_ready/searchdb_coocurence_colapsedonescherichiaspecieslevel_new.csv", "r") as handler:
#     for line in handler:
#         ls = line.strip().split('\t')
#         ColapsedOnEscherichiaSpeciesLevel(gene1=ls[1], gene2=ls[2], together=ls[3], first_only=ls[4], second_only=ls[5],
#                                      neither=ls[6], pvalue=ls[7], data_base='n').save()

# # print("Escherichia strain within species")
# with open("scripts_ready/searchdb_coocurence_colapsedonescherichiastrainwithingspecies_new.csv", "r") as handler:
#     for line in handler:
#         ls = line.strip().split('\t')
#         ColapsedOnEscherichiaStrainWithingSpecies(gene1=ls[1], gene2=ls[2], together=ls[3], first_only=ls[4], second_only=ls[5],
#                                      neither=ls[6], pvalue=ls[7], data_base='e').save()


# #Legionella

# print("Legionella species")
# with open("import_data/searchdb_coocurence_colapsedonspecieslevel", "r") as handler:
#     for line in handler:
#         ls = line.strip().split('\t')
#         ColapsedOnSpeciesLevel(gene1=ls[0], gene2=ls[1], together=ls[2], first_only=ls[3], second_only=ls[4],
#                                      neither=ls[4], pvalue=ls[5], data_base='Collapsed on Legionella species').save()

# print("Legionella strains")
# with open("scripts_ready/searchdb_coocurence_colapsedonlegionellastrains_new.csv", "r") as handler:
#     for line in handler:
#         ls = line.strip().split('\t')
#         ColapsedOnLegionellaStrains(gene1=ls[1], gene2=ls[2], together=ls[3], first_only=ls[4], second_only=ls[5],
#                                      neither=ls[6], pvalue=ls[7], data_base='w').save()

print("Legionella strain within species")
with open("scripts_ready/searchdb_coocurence_colapsedonlegionellastrainwithingspecies_new.csv", "r") as handler:
    for line in handler:
        ls = line.strip().split('\t')
        ColapsedOnLegionellaStrainWithingSpecies(gene1=ls[1], gene2=ls[2], together=ls[3], first_only=ls[4], second_only=ls[5],
                                     neither=ls[6], pvalue=ls[7], data_base='q').save()

