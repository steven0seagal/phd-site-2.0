import psycopg2

# Connect to PostgreSQL database
# conn = psycopg2.connect(database="your_database_name", user="your_username", password="your_password", host="your_host", port="your_port")
conn = psycopg2.connect(user = "hello_django", password = "hello_django",	host = "95.179.229.40",port = "5432", database = "hello_django_dev")

cursor = conn.cursor()

# Open and read the file
with open("searchdb_coocurence_colapsedonspecieslevel_uniqe.csv") as file:
    for line in file:
        # Split the line by tabs
        columns = line.strip().split('\t')
        
        # Extract each column
        col1 = columns[1]
        col2 = columns[2]
        col3 = int(columns[3])
        col4 = int(columns[4])
        col5 = int(columns[5])
        col6 = int(columns[6])
        col7 = float(columns[7])
        col8 = 'n'
        
        # Insert the row into PostgreSQL
        cursor.execute("INSERT INTO searchdb_coocurence_colapsedonspecieslevel (gene1, gene2, together, first_only, second_only, neither, pvalue, data_base) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (col1, col2, col3, col4, col5, col6, col7,col8))

# Commit the changes and close the connection
conn.commit()
conn.close()

