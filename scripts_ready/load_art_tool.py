from art.models import ResultsHmm

with open('important_files/art/art_tool','r') as handler:
    data = [x.strip().split() for x in handler]


for line in data[1:]:
    ResultsHmm(file_name=line[0],
               hmm_name=line[1],
               family_id=line[2],
               family_name_in_results=line[3],
               pfam_in_results = line[4],
               link_to_family_site=line[5]).save()