from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from .models import Family, Clade, InterPro, Structure, Taxonomy, GeneOntology, ExternalDB, KeywordFamily, Citation, \
    Tasks, WebLogo, ResultsHmm, MatchingSeq,FullSeq, DomainSeq, HMMmodels
from django.db.models import Q
import uuid
from django.core.exceptions import ObjectDoesNotExist
from django.utils.safestring import mark_safe
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

def art_main(request):
    return (render(request, 'art/main.html'))


def single_record(request):
    return (render(request, 'art/single_family.html'))


def list_of_families(request):

    context = {}
    context['results'] = {}
    clades = Clade.objects.all()
    for clade in clades:
        context['results'][clade.clade_id] = {'name': clade.clade_name, 'clade_description':clade.description, 'families':None}
        
        if request.user.id in [1,9,12]:
            families = Family.objects.filter(clade=clade)
        else:
            families = Family.objects.filter(clade=clade, public=True)
        
        if len(families) > 0:
            context['results'][clade.clade_id]['families'] = []
        for family in families:
            context['results'][clade.clade_id]['families'].append(
                {'family_name': family.family_name, 'family_id': family.family_id})


    return render(request,'art/database.html',context)


def result_search(request):
    context = {}
    query = request.GET.get('q')
    if query == None:
        context['family'] = None
    else:
        # FAMILY
        family_id_result = Family.objects.filter(Q(family_id__icontains=query), public=True)
        if len(family_id_result) > 0:
            context['family_id'] = []
            for i in family_id_result:
                context['family_id'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_id_count'] = len(family_id_result)

        # FAMILY NAME
        family_name_result = Family.objects.filter(Q(family_name__icontains=query), public=True)
        if len(family_name_result) > 0:
            context['family_name'] = []
            for i in family_name_result:
                context['family_name'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_name_count'] = len(family_name_result)

        # FAMILY DESCRIPTION
        family_description = []
        family_description_result = Family.objects.filter(Q(description__icontains=query), public=True)
        if len(family_description_result) > 0:
            context['family_description'] = []
            for i in family_description_result:
                if i.family_name not in family_description:
                    family_description.append(i.family_name)
                    context['family_description'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_description_count'] = len(family_description)

        # FAMILY CERTIFICATE
        family_certificate = []
        family_certificate_result = Family.objects.filter(Q(certificate__icontains=query), public=True)
        if len(family_certificate_result) > 0:
            context['family_certificate'] = []
            for i in family_certificate_result:
                if i.family_name not in family_certificate:
                    family_certificate.append(i.family_name)
                    context['family_certificate'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_certificate_count'] = len(family_certificate)

        # KEYWORD
        family_keyword = []
        keyword_result = Family.objects.filter(Q(keywordfamily__keyword__icontains=query), public=True)
        if len(keyword_result) > 0:
            context['family_keyword'] = []
            for i in keyword_result:
                if i.family_name not in family_keyword:
                    family_keyword.append(i.family_name)
                    context['family_keyword'].append({'family_name':i.family_name, 'id':i.family_id})
        context['family_keyword_count'] = len(family_keyword)

    return (render(request, 'art/results.html', context))


class ListOfFamilies(TemplateView):
    template_name = '../templates/art/database.html'

    # model = Family

    def get_context_data(self, **kwargs):
        
        context = super().get_context_data(**kwargs)
        context['results'] = {}
        clades = Clade.objects.all()
        for clade in clades:
            context['results'][clade.clade_id] = {'name': clade.clade_name, 'clade_description':clade.description, 'families':None}
            families = Family.objects.filter(clade=clade, public=True)
            if len(families) > 0:
                context['results'][clade.clade_id]['families'] = []
            for family in families:
                context['results'][clade.clade_id]['families'].append(
                    {'family_name': family.family_name, 'family_id': family.family_id})

        return context


class SingleResult(TemplateView):
    template_name = '../templates/art/single_family.html'

    def get_context_data(self, family_id, **kwargs):

        context = super().get_context_data(**kwargs)

        family_data = Family.objects.get(family_id=family_id)
        if family_data.public == False:
            # if self.request.user.id not in [1,9,12]:
            return {'public':False}
        context['family'] = {}
        context['clade'] = {}
        context['interpro'] = {}
        context['structures'] = []
        context['taxonomy'] = []
        context['geneontology'] = []
        context['external'] = []
        context['keyword'] = []
        context['citation'] = []
        context['weblogo'] = {}
        context['aligned'] = {}
        context['unaligned'] = {}
        context['full'] = {}
        
        # FAMILY
        
        context['family']['name'] = family_data.family_name
        context['family']['id'] = family_data.family_id
        context['family']['description'] = mark_safe(family_data.description)
        context['family']['certificate'] = mark_safe(family_data.certificate)
        context['public'] = family_data.public
        # CLADE
        clade_data = family_data.clade.all()[0]
        context['clade']['name'] = clade_data.clade_name
        context['clade']['id'] = clade_data.clade_id

        # INTERPRO
        try:
            interpro_data = InterPro.objects.get(family=family_data)
            context['interpro']['id'] = interpro_data.inter_id
            context['interpro']['abstract'] = interpro_data.abstract
        except ObjectDoesNotExist:
            context['interpro']['id'] = None
            context['interpro']['abstract'] = None
        # STRUCTURES
        try:
            structures_data = Structure.objects.filter(family=family_data)
            for i in structures_data:
                context['structures'].append({'pdb_id': i.pdb_id, 'protein_name': i.protein_name, 'file': i.pdb_file})
        except ObjectDoesNotExist:
            context['structures'].append({'pdb_id': None, 'protein_name': None, 'file': None})

        # TAXONOMY
        try:
            taxonnomy_data = Taxonomy.objects.filter(family=family_data)
            for i in taxonnomy_data:
                context['taxonomy'].append({'tax_id': i.tax_id, 'tax_level': i.tax_level, 'tax_name':i.tax_name})
        except ObjectDoesNotExist:
            context['taxonomy'].append({'tax_id': None})


        # Geneontology
        try:
            geneontology_data = GeneOntology.objects.filter(family=family_data)
            for i in geneontology_data:
                context['geneontology'].append({'go_id': i.go_id, 'go_name': i.go_name, 'go_category': i.go_category})
        except ObjectDoesNotExist:
            context['geneontology'].append({'go_id': None, 'go_name': None, 'go_category': None})

        # EXTERNAL
        try:
            external_data = ExternalDB.objects.filter(family=family_data)
            for i in external_data:
                context['external'].append({'external_source': i.source, 'external_site': i.site, 'external_id': i.db_id})
        except ObjectDoesNotExist:
            context['external'].append({'external_source': None, 'external_site': None, 'external_id': None})

        # KEYWORD
        try:
            keyword_data = KeywordFamily.objects.filter(family=family_data)
            for i in keyword_data:
                context['keyword'].append(i)
        except ObjectDoesNotExist:
            context['keyword'].append(None)

        # CITATION
        try:
            citation_data =Citation.objects.filter(family=family_data)
            for i in citation_data:
                context['citation'].append({'pmid':i.pmid, 'title':i.title, 'author':i.author,'scimag':i.sci_mag})
        except ObjectDoesNotExist:
            context['citation'].append({'pmid':None, 'title':None, 'author':None, 'sci_mag':None})

        # WEBLOGO
        try:
            weblogo_data = WebLogo.objects.get(family= family_data)
            context['weblogo'] = {'image':weblogo_data.file}
        except ObjectDoesNotExist:
            context['weblogo'] = None
        # Aligned

        try:
            aligned_data = MatchingSeq.objects.get(family=family_data)
            context['aligned'] = {'file':aligned_data.file}
        except ObjectDoesNotExist:
            context['aligned'] = None

        # Domain
        try:
            domain_data = DomainSeq.objects.get(family=family_data)
            context['unaligned'] = {'file':domain_data.file}
        except ObjectDoesNotExist:
            context['unaligned'] = None
        # FULL_
        try:
            full_data = FullSeq.objects.get(family=family_data)
            context['full'] = {'file':full_data.file}
        except ObjectDoesNotExist:
            context['full'] = None

        # HMM
        try:
            full_data = HMMmodels.objects.get(family=family_data)
            context['hmm'] = {'file':full_data.hmm_file}
        except ObjectDoesNotExist:
            context['hmm'] = None

        return context



def searchsequence(request):
    return render(request, 'art/search_seq.html')

def result_check(request):
    if 'job_id' in request.GET:

        if request.GET['job_id']:
            job = request.GET['job_id']
            return redirect('hmmresult/{}'.format(job))
    else:
        return render(request, 'art/browse_result.html')

def query_sequence(request):
    sequence = request.GET['sequence']
    if sequence[0] == '>':
        sequence = sequence.split('\n')[1:]
        sequence = ''.join([line.strip() for line in sequence])
    else:
        sequence = ''.join([line.strip() for line in sequence])
        
    uniqe_id =uuid.uuid4()
    Tasks(sequence=sequence, user_id=1, uniqe_id=uniqe_id).save()
    return redirect('hmmresult/{}'.format(uniqe_id))

def search_hmm_result(hmm_name_in_file):

    result = ResultsHmm.objects.get(hmm_name= hmm_name_in_file)
    return result


def pares_hmm_result(job_id):
    with open(f'/app/mediafiles/results/{job_id}','r') as handler:
        data = [x.strip().split() for x in handler]
    no_header = [x for x in data if x[0][0] != '#']
    complete_result = []
    for line in no_header:
        database_obj = search_hmm_result(line[0])
        
        complete_result.append({'family_id':database_obj.family_id, 'score':line[4], 'family_name_in_results':database_obj.family_name_in_results, 'pfam':database_obj.pfam_in_results})
        # complete_result.append('\t E.value: '.join([line[0],line[4]]))

    return complete_result



class HmmResult(TemplateView):
    template_name = '../templates/art/hmmresult.html'

    def get_context_data(self, result_id, **kwargs):

        context = {}
        context['result_id'] = result_id
        database_data = Tasks.objects.filter(uniqe_id=result_id)
        if len(database_data) == 0:
            context['status'] = 'NotFound'
            return context
        elif len(database_data) == 1:
            result = database_data[0]
            if result.status == 'Pending':
                context['status'] = 'Pending'
                return context
            elif result.status == 'Error':
                context['status'] = 'Error'
                return context
            elif result.status == 'Done':
                context['status'] = 'Done'
                context['file'] = '/media/results/{}'.format(result_id)
                context['results'] = pares_hmm_result(result_id)




                return context


class GetTaskToRun(APIView):
    def get(self, request):
        """
        GET request to get the task to run
        :param request:
        :return:
        """
        task = Tasks.objects.filter(status='Pending').first()
        if task:
            task.status = 'Running'
            task.save()
            return Response({'sequence': task.sequence, 'job_id': task.uniqe_id}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'No task to run'}, status=status.HTTP_404_NOT_FOUND)

class UploadFile(APIView):
    """
    Upload file to server
    """

    def post(self, request):
        """
        POST request to upload file
        :param request:
        :return:
        """

        fileobjects = request.FILES.getlist("fileobjects")
        unique_id = request.POST.get('unique_id')
        print(unique_id)
        for file in fileobjects:
            # save file
            with open(f'/app/mediafiles/results/{unique_id}', 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            print(file)


        task = Tasks.objects.get(uniqe_id=unique_id)
        task.uploaded = True
        task.status = 'Done'
        task.save()

        return Response(status=status.HTTP_200_OK)


class ChangeStatus(APIView):
    def post(self, request):
        """
        POST request to change the status of the task
        :param request:
        :return:
        """
        task_id = request.data.get('task_id')
        status = request.data.get('status')
        task = Tasks.objects.get(uniqe_id=task_id)
        task.status = status
        task.save()
        return Response(status=status.HTTP_200_OK)


