from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .views import *
urlpatterns = [
    path('', art_main, name='art_main'),
    path('single_record', single_record, name='art_single_record'),
    path('results', result_search, name='art_search_result'),
    # path('families', ListOfFamilies.as_view(),name='art_families'),
    path('families', list_of_families,name='art_families'),
    path('family/<str:family_id>', SingleResult.as_view(), name='single_art'),
    path('searchsequence', searchsequence, name='search_sequence'),
    path('hmmresult/<str:result_id>', HmmResult.as_view(), name='hmm_result'),
    path('hmmresult',result_check, name='browse_result'),
    path('ass',query_sequence, name='query_sequence'),
    path('api/get-task', GetTaskToRun.as_view(), name='get_task'),
    path('api/upload-task', UploadFile.as_view(), name='upload_task'),
    path('api/upldate-status', ChangeStatus.as_view(), name='change_status')


]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
