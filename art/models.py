from django.db import models


class Tasks(models.Model):

    user_id = models.IntegerField(blank=True)
    sequence = models.TextField()
    status = models.CharField(max_length=255,default='Pending')
    result_file_1 = models.CharField(max_length=255,blank=True)
    result_file_2 = models.CharField(max_length=255,blank=True)
    created_time = models.DateTimeField(auto_now_add=True)
    uniqe_id = models.CharField(max_length=255,blank=True)
    uploaded = models.BooleanField(default=False)
    def __str__(self):
        return self.uniqe_id

class Clade(models.Model):
    clade_id = models.CharField(max_length=255)
    clade_name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    def __str__(self):
        return self.clade_name

class Family(models.Model):
    clade = models.ManyToManyField(Clade)
    family_id = models.CharField(max_length=255, db_index=True)
    family_name = models.CharField(max_length=255, db_index=True)
    description = models.TextField(null=True, blank=True)
    certificate = models.TextField(blank=True, null=True)
    public = models.BooleanField(default=False)

    def __str__(self):
        return self.family_name


class InterPro(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    inter_id = models.CharField(max_length=255)
    abstract = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.inter_id


class GeneOntology(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    go_id = models.CharField(max_length=255)
    go_name = models.CharField(max_length=255)
    go_category = models.CharField(max_length=255)

    def __str__(self):
        return self.go_name


class KeywordFamily(models.Model):
    family = models.ManyToManyField(Family)
    keyword = models.CharField(max_length=255, db_index=True)
    table_category = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.keyword


class Citation(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    pmid = models.CharField(max_length=255)
    title = models.CharField(max_length=255, null=True, blank=True)
    author = models.CharField(max_length=255, null=True, blank=True)
    sci_mag = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.pmid


class Structure(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    pdb_id = models.CharField(max_length=255)
    protein_name = models.CharField(max_length=255)
    pdb_file = models.CharField(max_length=255)
    merged_file = models.CharField(max_length=255)

    def __str__(self):
        return self.pdb_id


class Models(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    id_seq = models.CharField(max_length=255)
    matching_model = models.TextField()
    matching_model_many = models.TextField()

    def __str__(self):
        return self.id_seq


class HMMmodels(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    hmm_family_id = models.CharField(max_length=255)
    hmm_file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class WebLogo(models.Model):
    image = models.ImageField(upload_to='photos/%Y/%m/%d/')
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    file = models.CharField(max_length=255)


class ExternalDB(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    source = models.CharField(max_length=255)
    site = models.CharField(max_length=255)
    db_id = models.CharField(max_length=255, db_index=True)

    def __str__(self):
        return self.site

class Taxonomy(models.Model):
    family = models.ManyToManyField(Family)
    tax_id = models.CharField(max_length=255)
    tax_level = models.CharField(max_length=255,null=True, blank=True)
    tax_name = models.CharField(max_length=255,null=True, blank=True)
    
class FullSeq(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class DomainSeq(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class MatchingSeq(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    family_name = models.CharField(max_length=255)
    file = models.CharField(max_length=255)

    def __str__(self):
        return self.family_name

class ResultsHmm(models.Model):

    file_name = models.CharField(max_length=255)
    hmm_name = models.CharField(max_length=255)
    family_id = models.CharField(max_length=255, blank=True, null=True)
    family_name_in_results = models.CharField(max_length=255,blank=True,null=True)
    pfam_in_results = models.CharField(max_length=255,blank=True,null=True)
    link_to_family_site = models.CharField(max_length=255, blank=True,null=True)

    def __str__(self):
        return self.hmm_name