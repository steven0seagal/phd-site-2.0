from django.contrib import admin

# Register your models here.
from .models import *


class TasksAdmin(admin.ModelAdmin):
    list_display = ('id','user_id','status','uniqe_id')
    list_display_links = ('id',)
    list_per_page = 25
    search_fields = ('user_id','status','uniqe_id')

admin.site.register(Tasks, TasksAdmin)

class CladeAdmin(admin.ModelAdmin):
    list_display = ('id', 'clade_id', 'clade_name')
    list_display_links = ('clade_id',)
    list_per_page = 25
    search_fields = ('clade_name',)


admin.site.register(Clade, CladeAdmin)


class FamilyAdmin(admin.ModelAdmin):
    list_display = ('family_id', 'family_name', 'public')
    list_display_links = ('family_id',)
    list_per_page = 25
    search_fields = ('family_name',)


admin.site.register(Family, FamilyAdmin)


class InterProAdmin(admin.ModelAdmin):
    list_display = ('inter_id',)
    list_display_links = ('inter_id',)
    list_per_page = 25
    search_fields = ('inter_id',)


admin.site.register(InterPro, InterProAdmin)


class GeneOntologyAdmin(admin.ModelAdmin):
    list_display = ('go_id', 'go_name', 'go_category')
    list_display_links = ('go_id',)
    list_per_page = 25
    search_fields = list_display


admin.site.register(GeneOntology, GeneOntologyAdmin)


class KeywordFamilyAdmin(admin.ModelAdmin):
    list_display = ('keyword',)
    list_display_links = list_display
    list_per_page = 25
    search_fields = list_display


admin.site.register(KeywordFamily, KeywordFamilyAdmin)


class CitationAdmin(admin.ModelAdmin):
    list_display = ('pmid','title', 'sci_mag')
    list_display_links = ('pmid',)
    list_per_page = 25
    search_fields = ('pmid', 'title')


admin.site.register(Citation, CitationAdmin)

class StructureAdmin(admin.ModelAdmin):
    list_display= ('id','pdb_id', 'protein_name')
    list_display_links = ('pdb_id',)
    list_per_page = 25
    search_fields = ('pdb_id',)


admin.site.register(Structure, StructureAdmin)

class ModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_seq')
    list_display_links = ('id_seq',)
    list_per_page = 25
    search_fields = ('id_seq',)

admin.site.register(Models, ModelAdmin)


class HMMModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'family_name', 'hmm_family_id')
    list_display_links = ('family_name',)
    list_per_page = 25
    search_fields = ('family_name','hmm_family_id')


admin.site.register(HMMmodels, HMMModelAdmin)


class WebLogoAdmin(admin.ModelAdmin):

    list_display = ('id', 'file')
    list_display_links = ('file',)
    list_per_page = 25
    search_fields = ('file',)

admin.site.register(WebLogo, WebLogoAdmin)


class ExternalDBAdmin(admin.ModelAdmin):
    list_display = ('source', 'site')
    list_display_links = ('source',)
    list_per_page = 25
    search_fields = ('source',)

admin.site.register(ExternalDB, ExternalDBAdmin)



class TaxonomyAdmin(admin.ModelAdmin):
    list_display = ('tax_id', )
    list_display_links = ('tax_id', )
    list_per_page = ('tax_id', )
    list_per_page = 25

admin.site.register(Taxonomy, TaxonomyAdmin)

class FullSeqAdmin(admin.ModelAdmin):
    list_display = ('family_name',)
    list_display_links = ('family_name',)
    list_per_page = ('family_name',)
    list_per_page = 25

admin.site.register(FullSeq, FullSeqAdmin)

class DomainSeqAdmin(admin.ModelAdmin):
    list_display = ('family_name',)
    list_display_links = ('family_name',)
    list_per_page = ('family_name',)
    list_per_page = 25

admin.site.register(DomainSeq, DomainSeqAdmin)

class MatchingSeqAdmin(admin.ModelAdmin):
    list_display = ('family_name',)
    list_display_links = ('family_name',)
    list_per_page = ('family_name',)
    list_per_page = 25

admin.site.register(MatchingSeq, MatchingSeqAdmin)

class ResultsHmmAdmin(admin.ModelAdmin):
    list_display = ('family_id',)
    list_display_links = ('family_id',)
    list_per_page = ('family_id',)
    list_per_page = 25

admin.site.register(ResultsHmm,ResultsHmmAdmin)