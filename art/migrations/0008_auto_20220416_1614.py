# Generated by Django 3.2.6 on 2022-04-16 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('art', '0007_tasks_uploaded'),
    ]

    operations = [
        migrations.AddField(
            model_name='taxonomy',
            name='tax_level',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='taxonomy',
            name='tax_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
