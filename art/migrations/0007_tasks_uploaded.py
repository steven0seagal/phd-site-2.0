# Generated by Django 3.2.6 on 2022-03-30 10:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('art', '0006_tasks_uniqe_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='tasks',
            name='uploaded',
            field=models.BooleanField(default=False),
        ),
    ]
