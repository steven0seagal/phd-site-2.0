from collections import Counter
from scipy.stats import wilcoxon as test
import pandas as pd
from statsmodels.stats.multitest import multipletests as correction  #
import datetime
import json
import math
import pdb
from scipy.stats import poisson


class NeighborhoodAnalyzerFromGene:

    def __init__(self, user_list_of_genes, user_distance_value, user_database, user_cutoff, user_correction,
                 user_strand_value,
                 user_output):
        self.user_list_of_genes = user_list_of_genes
        self.user_distance_value = user_distance_value
        self.user_database = user_database
        self.user_cutoff = user_cutoff
        self.user_correction = user_correction
        self.user_strand_value = user_strand_value
        self.user_output = user_output

    def open_single_column_file(self, file_name):
        """ Opens file that contains one column of data """
        plik = []
        with open(file_name) as inputfile:
            for line in inputfile:
                plik.append(line.strip())
        return plik

    def open_multiple_column_file(self, file_name, split_mark=" "):
        """ Opens file that contain more than one column and split it by space. """
        plik = []
        with open(file_name) as inputfile:
            for line in inputfile:
                plik.append(line.strip().split(split_mark))
        return plik

    def open_singleline_num(self, path_to_file):
        """ Opens file that contain 1 column and strip it by space. """

        with open(path_to_file, 'r') as f:
            file_names = [float(line.strip()) for line in f]
        return file_names

    def open_json_file(self, file_name):
        with open(file_name, 'r') as fp:
            data = json.load(fp)
        all_data = []
        for key, values in data.items():
            all_data.append([key, values[0], values[1]])
        return all_data

    def open_query(self, path_to_file):

        with open(path_to_file, 'r') as f:
            file_names = [line.strip() for line in f]
        return file_names

    def save_complete(self, file, message_for_additional_data, complete_data):
        """ Saves all stuff together as one file """
        complete_data.index.name = 'Domain'
        complete_data.to_csv('/home/djangoadmin/final_site-project' + file, sep='\t', mode='w')


    def collect_pfam_domain_description(self, file):
        """ Opens file only to gather information"""

        data = {}
        with open(file) as inputfile:
            for line in inputfile:
                try:

                    one_line = line.strip().split("@")
                    domena = one_line[0]
                    pre_family = one_line[1][8:]
                    delete_this = "(" + domena.upper() + ")"
                    family = pre_family.replace(delete_this, "")
                    summary = one_line[2][9:]
                    data[domena] = (family, summary)

                except IndexError:
                    continue
        return data

    def create_6_lists(self, file):

        # data = self.open_multiple_column_file(file)
        start_coord = []
        end_coord = []
        orientation = []
        domains = []
        genes = []
        contig = []

        with open("/home/djangoadmin/final_site-project/important_files/nowa_baza_danych/" + file) as inputfile:
            for line in inputfile:
                bit = line.strip().split()

                genes.append(int(bit[0]))
                start_coord.append(int(bit[1]))
                end_coord.append(int(bit[2]))
                orientation.append(bit[3])
                domains.append(bit[4])
                contig.append(bit[5])
        return genes, start_coord, end_coord, orientation, domains, contig

    def find_correct_genome(self, gene, reference_data):

        for line in reference_data:
            genome = line[0]
            starts = [x for x in line[1::2]]
            ends = [x for x in line[2::2]]
            counter = 0
            for start in starts:
                end = ends[counter]
                counter += 1
                if int(gene) in range(int(start), int(end) + 1):
                    return genome

    def get_genome_size_in_gene(self, genome_id, list_of_genome, list_of_size):
        """ Takes genome's id, list of genome and lists with data about how much genes in genome"""
        indeks_genomu = list_of_genome.index(genome_id)
        genome_size_in_gene = list_of_size[indeks_genomu]
        return genome_size_in_gene

    def search_for_gene_in_genome(self, q_gene, genes, start_coords, end_coords, orients, contig):
        result = []
        main_index = genes.index(int(q_gene))
        result.append(start_coords[main_index])
        result.append(end_coords[main_index])
        result.append(orients[main_index])
        result.append(contig[main_index])
        result.append(q_gene)
        return result

    def get_range_coordinates(self, target_gene, distance):
        """
        Based on how large neighbourhood user wants to analyze creates
        points FROM and TO, additionaly shows where main user's pfam begins and ENDS
        with orientation on strands
        """

        gene_id = target_gene[4]
        gene_beg = target_gene[0]
        gene_end = target_gene[1]
        searched_gene_orient = target_gene[2]
        last_coordinate = gene_end + int(distance)
        first_coordinate = gene_beg - int(distance)

        return last_coordinate, first_coordinate, gene_beg, gene_end, searched_gene_orient

    def get_domain_both_strands(self, start_coord, end_coord, last_coordinate, first_coordinate, gene_beg,
                                gene_end, contig, result):
        pfam_index_to_neigh = []
        s_coord_counter = 0
        for s_coord in start_coord:
            if s_coord <= last_coordinate and s_coord >= gene_beg and contig[s_coord_counter] == result[
                3] and s_coord_counter not in pfam_index_to_neigh:
                pfam_index_to_neigh.append(s_coord_counter)
                s_coord_counter += 1
            else:
                s_coord_counter += 1
                continue
        e_coord_counter = 0
        for e_coord in end_coord:
            if e_coord >= first_coordinate and e_coord <= gene_end and contig[e_coord_counter] == result[
                3] and e_coord_counter not in pfam_index_to_neigh:
                pfam_index_to_neigh.append(e_coord_counter)
                e_coord_counter += 1
            else:
                e_coord_counter += 1
                continue
        return pfam_index_to_neigh

    def get_domain_same_strand(self, start_coord, end_coord, last_coordinate, first_coordinate, gene_beg,
                               gene_end, contig, result):
        pass

    def get_domain_oposite_strand(self):
        pass

    def get_domain_both_zeros(self, ref_gene, q_gene):

        index_list_gene = []
        counter = 0
        for gene in ref_gene:
            if gene == int(q_gene):
                index_list_gene.append(counter)
            counter += 1
        return index_list_gene

    def get_list_of_genes_in_neigh(self, list_of_genes, index_list):
        """ Takes overall gene's list in genome and creates complete list of genes in neighbourhood """

        list_of_genes_in_neigh = []
        for pfam_domain in index_list:
            list_of_genes_in_neigh.append(list_of_genes[pfam_domain])
        return list_of_genes_in_neigh

    def get_size_in_genes(self, genome_or_gene_list):
        """ Takes list of genes and returns size of neighbourhood """

        list_of_genes = []
        for gene in genome_or_gene_list:
            list_of_genes.append(gene)
        return len(set(list_of_genes))

    def get_list_of_domain_in_neigh(self, pfam_index, gene, domains):

        to_counter = []
        party = []
        party.append(gene)
        for part in pfam_index:
            party.append(domains[part])
            to_counter.append(domains[part])
        #        neighbourhood_complete.append(party)
        # pdb.set_trace()
        return to_counter

    def multiple_test_correction(self, some_dataframe, correction_met):

        if correction_met == 'none':
            return some_dataframe
        else:
            try:

                value_to_correct = [float(x) for x in some_dataframe.Poisson.tolist()]
                reject, pvals_corrected, alphaSidak, alphaBonf = correction(pvals=value_to_correct,
                                                                            method=correction_met,
                                                                            is_sorted=False, returnsorted=False)
                pvals = pvals_corrected.tolist()
                some_dataframe.Poisson = pvals
                return some_dataframe
            except ZeroDivisionError:
                pass

    def multiple_test_correction_list(self, some_tuple_list, correction_met):
        pvalues = [float(x) for x in some_tuple_list]

        if correction_met == 'none':
            output = pvalues

        else:
            try:

                reject, pvals_corrected, alphaSidak, alphaBonf = correction(pvals=pvalues,
                                                                            method=correction_met,
                                                                            is_sorted=False, returnsorted=False)
                pvals_after_corr = pvals_corrected.tolist()
                output = pvals_after_corr
            except ZeroDivisionError:
                output = pvalues
        return output

    def cutoff_value(self, some_dataframe, cutoff):

        domain_list = list(some_dataframe.index)
        if cutoff == 'none':
            return some_dataframe
        elif cutoff == '0':
            for i in domain_list:
                diff = float(some_dataframe.loc[i, 'Poisson'])
                if diff < 0 and diff > 0:
                    some_dataframe = some_dataframe.drop([i])
            return some_dataframe
        else:
            cutoff = float(cutoff)
            for i in domain_list:
                diff = float(some_dataframe.loc[i, 'Poisson'])
                if diff > cutoff:
                    some_dataframe = some_dataframe.drop([i])
            return some_dataframe

    def sort_table(self, some_dataframe):

        sorted_data = some_dataframe.sort_values('Poisson', ascending=True)
        return sorted_data

    def add_information(self, some_dataframe, dictionary):

        indeksy = list(some_dataframe.index)
        for i in indeksy:
            try:

                pfam = i[0:2] + i[4:]
                family = dictionary[pfam][0]
                summary = dictionary[pfam][1]

                some_dataframe.at[i, 'Family'] = family
                some_dataframe.at[i, 'Summary'] = summary
            except KeyError:
                continue
        return some_dataframe

    def pfam_for_pf(self, dataframe):
        """Final customization of dataframe
            --> PF02696 instead of pfam02696
            --> PVALUE in 10e-3 format
            --> In what percentage % format
            --> avg occurences and density  have now 3 digits after coma
            --> drop NOam
            -->
        """

        indexy = dataframe.index
        for i in indexy:
            dataframe = dataframe.rename(index={i: i[:2].upper() + i[4:]})
        dataframe['Poisson'] = dataframe['Poisson'].map('{:.3e}'.format)
        dataframe['In what percentage'] = dataframe['In what percentage'].map('{:.3%}'.format)
        dataframe = dataframe.round({"average occurence in neighbourhood": 3, "average occurence in genome": 3,
                                     "Density difference": 3})
        if "NOam" in indexy:
            dataframe = dataframe.drop(index="NOam")
        # dataframe = dataframe.style.format({'In what percentage': '{:,.3%}'.format})
        dataframe.drop('average occurence in neighborhood', axis=1, inplace=True)
        dataframe.drop('average occurence in genome', axis=1, inplace=True)
        dataframe.drop('avg neigh size', axis=1, inplace=True)
        dataframe.drop('domain in whole database', axis=1, inplace=True)
        if self.user_correction == 'none':
            dataframe.drop('Poisson_correct', axis=1, inplace=True)
        else:
            dataframe.drop('Poisson', axis=1, inplace=True)

        return dataframe

    def collect_gene_without_genome(self, gene):
        with open('/home/djangoadmin/final_site-project/important_files/missing_genes/data_to_download.txt',
                  'a+') as output:
            output.write(gene)
            output.write("\n")

    def make_genera_statistic(self, genome_list):

        with open("/home/djangoadmin/final_site-project/important_files/genera_statistics", "r") as handler:
            data = [x.strip().split() for x in handler]  # data to lista list [[genom,genus],[genome,genus]]
        genomes = [x[0] for x in data]
        genera = [x[1] for x in data]

        just_genera = []

        for genome in genome_list:
            indeks = genomes.index(genome)
            just_genera.append(genera[indeks])
        counter_genera = Counter(just_genera)

        zliczanie = []
        for genus, value in counter_genera.items():
            zliczanie.append((genus, value / len(genome_list)))
        return zliczanie

    def poisson_distribution(self, p, n, domain, percentage_counter):

        if int(percentage_counter[domain]) < p * n:
            return 1
        else:
            sums = 0
            k = 0
            number_of_samples = int(percentage_counter[domain])
            while k < number_of_samples:
                sums += poisson.pmf(k=k, mu=n*p)
                k+=1
            if 1 - sums >= 0.0:
                return 1 - sums
            else:
                return 0.0


            # stat = poisson.pmf(k=int(percentage_counter[domain]), mu=n * p)
            # return stat

    def go(self):
        ################################################################################################################
        # VULTR
        query_data = self.open_query("/home/djangoadmin/final_site-project" + self.user_list_of_genes)

        domain_information = self.collect_pfam_domain_description(
            "/home/djangoadmin/final_site-project/important_files/domain_information")
        genome_gene_reference = self.open_multiple_column_file(
            "/home/djangoadmin/final_site-project/important_files/genomes_map")
        genome_size_in_gene = self.open_multiple_column_file(
            "/home/djangoadmin/final_site-project/important_files/GENOME_ID_SIZE_IN_GENE.txt")
        with open('/home/djangoadmin/final_site-project/important_files/counter_domains', 'r') as handler:
            pfam_counter_domains = json.load(handler)
        ################################################################################################################
        ################################################################################################################
        # # LOCAL

        # domain_information = self.collect_pfam_domain_description(
        #     '/mnt/d/45.76.38.24/final_site-project/important_files/domain_information')
        # # open file about gene in genomes [genome min(gene) max(gene)]
        # genome_gene_reference = self.open_multiple_column_file(
        #     '/mnt/d/45.76.38.24/final_site-project/important_files/genomes_map')
        # # open file about genome size in gene and split to 2 lists
        # genome_size_in_gene = self.open_multiple_column_file(
        #     '/mnt/d/45.76.38.24/final_site-project/important_files/GENOME_ID_SIZE_IN_GENE.txt')
        ################################################################################################################
        ################################################################################################################
        # # KLASTER
        # domain_information = self.collect_pfam_domain_description(
        #     '/home/klaster/ProFaNA v1.0/important_files/domain_information')
        # genome_gene_reference = self.open_multiple_column_file(
        #     '/home/klaster/ProFaNA v1.0/important_files/genomes_map')
        # genome_size_in_gene = self.open_multiple_column_file(
        #     '/home/klaster/ProFaNA v1.0/important_files/GENOME_ID_SIZE_IN_GENE.txt')
        ################################################################################################################

        print("Checkpoint #1 Data loaded", datetime.datetime.now())
        genome_ids = [x[0] for x in genome_size_in_gene]
        genome_size = [x[1] for x in genome_size_in_gene]
        genome_size_in_gene = None
        just_neigh_data = []
        genomes_with_domains = []
        neigh_genome_size = []
        counter = 0
        counter_db = 0
        sum_of_genes_in_neighborhoods = 0
        number_of_neighborhoods = 0
        genome_number_to_stat = 0
        print("DLOK TIME START", datetime.datetime.now())
        # Loop through list of genes and do all stuff

        percentage_counter = Counter()
        for gene in query_data:
            counter_db += 1
            counter += 1
            try:

                correct_genome = self.find_correct_genome(gene, genome_gene_reference)
                correct_size = self.get_genome_size_in_gene(correct_genome, genome_ids, genome_size)
                ref_gene, ref_start_coord, ref_end_coord, ref_orient, ref_domain, ref_contig = self.create_6_lists(
                    correct_genome)
                search_result = self.search_for_gene_in_genome(gene, ref_gene, ref_start_coord, ref_end_coord,
                                                               ref_orient,
                                                               ref_contig)
                if self.user_distance_value != 0:
                    last_coordiate, first_coordinate, gene_beg, gene_end, searched_gene_orientation, = \
                        self.get_range_coordinates(search_result, self.user_distance_value)
                    pfam_index_to_neigh = self.get_domain_both_strands(ref_start_coord, ref_end_coord, last_coordiate,
                                                                       first_coordinate, gene_beg, gene_end, ref_contig,
                                                                       search_result)

                else:
                    pfam_index_to_neigh = self.get_domain_both_zeros(ref_gene, search_result[4])

                genes_in_neigh = self.get_list_of_genes_in_neigh(ref_gene, pfam_index_to_neigh)
                number_of_genes_in_neigh = self.get_size_in_genes(genes_in_neigh)
                whole_neigh = self.get_list_of_domain_in_neigh(pfam_index_to_neigh, gene, ref_domain)
                just_neigh_data.append(whole_neigh)
                genomes_with_domains.append(correct_genome)
                neigh_genome_size.append((number_of_genes_in_neigh, correct_size))
                percentage_counter += Counter(whole_neigh)
                genome_number_to_stat += 1
                sum_of_genes_in_neighborhoods += int(number_of_genes_in_neigh)
                number_of_neighborhoods += 1
            except ValueError:
                self.collect_gene_without_genome(gene)
                continue
        print("DLOK TIME OVER", datetime.datetime.now())
        print("concatenate_domain")
        alls = []
        neigh_counter = Counter()

        for i in just_neigh_data:
            for j in i:
                if j not in alls:
                    alls.append(j)
        genome_counter = Counter()

        print("MATRIX CREATING", datetime.datetime.now())
        counter = 0
        for domain_list, genome_in_domain, ng_size in zip(just_neigh_data, genomes_with_domains, neigh_genome_size):
            counter += 1
            genes, start_coord, end_coord, orientation, domains, contig = self.create_6_lists(genome_in_domain)
            genome_domains_counter = Counter(domains)
            genome_counter += genome_domains_counter
            neigh_domains_counter = Counter(domain_list)
            neigh_counter += neigh_domains_counter

        print("MATRIX OVER", datetime.datetime.now())

        print("Wilcoxon start", datetime.datetime.now())

        najlepsze_dane = pd.DataFrame(columns=['occurence in neighborhoods',
                                               'average occurence in neighborhood', 'occurence genomes',
                                               'average occurence in genome', 'In what percentage', 'Poisson','Poisson_correct' 'P', 'N',
                                               'K',
                                               'avg neigh size', 'domain in whole database', 'Family', 'Summary'])

        counter = 0
        scores = []
        # percentage = []
        scores_poisson = []
        for i in alls:
            counter += 1
            # data_to_calculate = self.open_singleline_num("/home/djangoadmin/final_site-project/scripts/temp_data/" + i)
            # for_percentage = [float(x) for x in data_to_calculate if x > 0]
            # percentage.append(len(for_percentage) / len(just_neigh_data))
            # wynik = test(data_to_calculate, zero_method="wilcox")
            # mean = sum(data_to_calculate) / len(just_neigh_data)
            # scores.append((wynik.pvalue, mean))

            p_KP = ((sum_of_genes_in_neighborhoods / genome_number_to_stat) * pfam_counter_domains[i]) / 326628067

            najlepsze_dane.at[i, 'P'] = p_KP
            najlepsze_dane.at[i, 'avg neigh size'] = sum_of_genes_in_neighborhoods / genome_number_to_stat
            najlepsze_dane.at[i, 'domain in whole database'] = pfam_counter_domains[i]
            najlepsze_dane.at[i, 'sum of database len'] = 326628067
            poisson_test_result = self.poisson_distribution(p_KP, number_of_neighborhoods, i, percentage_counter)
            najlepsze_dane.at[i, 'K'] = int(percentage_counter[i])
            najlepsze_dane.at[i, 'N'] = number_of_neighborhoods
            najlepsze_dane.at[i, 'P'] = p_KP
            najlepsze_dane.at[i, 'Poisson'] = poisson_test_result
            scores_poisson.append(poisson_test_result)
        scores_after_correction = self.multiple_test_correction_list(scores_poisson, self.user_correction)
        print("Wilcoxon OVER", datetime.datetime.now())

        print("Collecting data START", datetime.datetime.now())

        # for domena, wynik in zip(alls, scores_after_correction):
        #     if self.user_distance_value == 0:
        #         if wynik[1] > 0:
        #             najlepsze_dane.at[domena, 'PVALUE'] = wynik[0]
        #             najlepsze_dane.at[domena, 'Density difference'] = wynik[1]
        #             najlepsze_dane.at[domena, 'In what percentage'] = percentage_counter[domena] / len(just_neigh_data)
        #         else:
        #             najlepsze_dane.at[domena, 'PVALUE'] = 1.0
        #             najlepsze_dane.at[domena, 'Density difference'] = wynik[1]
        #             najlepsze_dane.at[domena, 'In what percentage'] = \
        #                 [domena] / len(just_neigh_data)
        #
        #     else:
        #
        #         if wynik[1] > 0:
        #             najlepsze_dane.at[domena, 'PVALUE'] = wynik[0]
        #             najlepsze_dane.at[domena, 'Density difference'] = wynik[1]
        #             najlepsze_dane.at[domena, 'In what percentage'] = percentage_counter[domena] / len(just_neigh_data)

        for domain,wynik in zip(najlepsze_dane.index.to_list(),scores_after_correction):
            najlepsze_dane.at[domain, 'Poisson_correct'] = wynik
            najlepsze_dane.at[domain, 'In what percentage'] = percentage_counter[domain] / len(just_neigh_data)
            najlepsze_dane.at[domain, 'occurence genomes'] = genome_counter[domain]
            najlepsze_dane.at[domain, 'average occurence in genome'] = genome_counter[domain] / len(
                set(genomes_with_domains))
            najlepsze_dane.at[domain, 'occurence in neighborhoods'] = neigh_counter[domain]
            najlepsze_dane.at[domain, 'average occurence in neighborhood'] = neigh_counter[domain] / len(
                just_neigh_data)

        print("Collecting data OVER", datetime.datetime.now())

        print("Customizing_data START", datetime.datetime.now())
        after_test = self.multiple_test_correction(najlepsze_dane, self.user_correction)
        after_cutoff = self.cutoff_value(najlepsze_dane, self.user_cutoff)
        sorted_table = self.sort_table(after_cutoff)
        added_information = self.add_information(sorted_table, domain_information)
        final_data = self.pfam_for_pf(added_information)
        print("Customizing_data OVER", datetime.datetime.now())
        message_for_additional_data = "Pfam domain ,occurence in neighbourhoods, " \
                                      "occurence genomes, " \
                                      "In what percentage?, Poisson," \
                                      "P, N, K, domain in whole database," \
                                      "Family, Summary"

        self.save_complete(self.user_output, message_for_additional_data, final_data)

