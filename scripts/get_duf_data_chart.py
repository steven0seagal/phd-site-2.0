import json
import os 
import pdb
with open("../media/pfam_duf_now","r") as handler:
    pfam_to_analyze = [x.strip() for x  in handler]


with open("../important_files/counter_domains",'r') as handler:
    domain_counter = json.load(handler)

# for i in pfam_to_analyze:
#     print(domain_counter[i])

with open("../important_files/all_genomes","r") as handler:
    files = [x.strip() for x  in handler]

for pfam in pfam_to_analyze:
    counter = 0
    
    for file in files:
        try:
            with open("../important_files/nowa_baza_danych/{}".format(file),"r") as handler:

                data = handler.read()
            if pfam in data:
                counter +=1
        except FileNotFoundError:
            continue
    print(counter, pfam)    

