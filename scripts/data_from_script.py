
def feedMe(object,sel_tool):
    output = []

    for part in object:


        loads_info  = part.script
        data = loads_info.split()

        if part.tool == sel_tool and sel_tool == 'NA':
            taxonomy_file = part.ontology_file.replace('ontology','taxonomy')
            
            domain = data[2][:2].upper() + data[2][4:]

            tax = data[4].capitalize() + ' (genus)'

            strand  = data[7].capitalize()
            file_view = part.file.split('/')[-1].split('.')[0]
            
            # file_view = data[8].split('/')[-1].split('.')[0]
            
            if data[6]  == 'bonferroni':
                correction = 'Bonferroni'
            elif data[6] == 'fdr_bh':
                correction = 'Benjamini-Hochberg Procedure'
            else:
                correction = 'None'
            one_job = {'id':part.id ,'domain':domain, 'range':data[3], 'cutoff':data[5], 'tax': tax,
                       'strand':strand, 'correction': correction, 'link': data[8],'status': part.status,
                       'out_name': part.analysis_name, 'is_favourite':part.is_favourite, 'created_at':part.created_at, 'ontology':part.ontology_file, 'file_view':file_view, 'taxonomy_file':taxonomy_file}
            output.append(one_job)
        elif part.tool == sel_tool and sel_tool == 'NAG':
            taxonomy_file = part.ontology_file.replace('ontology','taxonomy')
            
            domain = "Gene list"
            tax = 'Whole database'
            strand  = data[7].capitalize()
            file_view = data[8].split('/')[-1].split('.')[0]
            if data[6]  == 'bonferroni':
                correction = 'Bonferroni'
            elif data[6] == 'fdr_bh':
                correction = 'Benjamini-Hochberg Procedure'
            else:
                correction = 'None'
            one_job = {'id':part.id ,'domain':domain, 'range':data[3], 'cutoff':data[5], 'tax': tax,
                       'strand':strand, 'correction': correction, 'link': data[8],'status': part.status,
                       'out_name': part.analysis_name,'is_favourite':part.is_favourite, 'created_at':part.created_at,'ontology':part.ontology_file,'taxonomy_file':taxonomy_file, 'file_view':file_view}
            output.append(one_job)

        elif part.tool == sel_tool and sel_tool == 'M3A':
            one_job = {'id':part.id, 'master_master':data[2], 'master_slave_1':data[3], 'master_slave_2':data[4],'status':part.status, 'link': data[5],'anal_name':part.analysis_name,'is_favourite':part.is_favourite, 'created_at':part.created_at, 'file_view':file_view}
            output.append(one_job)

        elif part.tool == sel_tool and sel_tool == 'NAF':
            taxonomy_file = part.ontology_file.replace('ontology','taxonomy')

            domain = data[2][:2].upper() + data[2][4:]
            tax = data[-6] + ' (family)'
            strand = data[7].capitalize()
            file_view = part.file.split('/')[-1].split('.')[0]
            if data[6]  == 'bonferroni':
                correction = 'Bonferroni'
            elif data[6] == 'fdr_bh':
                correction = 'Benjamini-Hochberg Procedure'
            else:
                correction = 'None'
            one_job = {'id': part.id, 'domain': domain, 'range': data[3], 'cutoff': data[5], 'tax': tax,
                       'strand': strand, 'correction': correction, 'link': part.file, 'status': part.status,
                       'out_name': part.analysis_name,'is_favourite':part.is_favourite, 'created_at':part.created_at,'ontology':part.ontology_file, 'file_view':file_view,'taxonomy_file':taxonomy_file}
            output.append(one_job)

        elif part.tool == sel_tool and sel_tool == "NAD":
            domain = data[2][:2].upper() + data[2][4:]
            taxonomy_file = part.ontology_file.replace('ontology','taxonomy')

            if len(data) == 10:
                tax = "All database"
                # print(data)
            else:
                # print(data)
                tax = "All database - {} ".format(data[9].capitalize())
            file_view = part.file.split('/')[-1].split('.')[0]
            strand = data[7].capitalize()
            if data[6] == 'bonferroni':
                correction = 'Bonferroni'
            elif data[6] == 'fdr_bh':
                correction = 'Benjamini-Hochberg Procedure'
            else:
                correction = 'None'
            one_job = {'id': part.id, 'domain': domain, 'range': data[3], 'cutoff': data[5], 'tax': tax,
                       'strand': strand, 'correction': correction, 'link': part.file, 'status': part.status,
                       'out_name': part.analysis_name,'is_favourite':part.is_favourite, 'created_at':part.created_at,'ontology':part.ontology_file, 'file_view':file_view,'taxonomy_file':taxonomy_file}
            output.append(one_job)

        elif part.tool == sel_tool and sel_tool == "FFAS":
            data = loads_info.split("&&")
            PDB_file = "-"
            SCOP_file = "-"
            PFAM_file = "-"
            Hsapiens_file = "-"
            COG_file = "-"
            VFDB_file = "-"
            VFDB_custom_file= "-"
            if len(data) == 1:

                small_data = data[0].split(" ")
                data = data[0].split(" ")
                one_job = {'id': part.id, 'out_name': part.analysis_name,'insert_file':small_data[3], 'profile_id':small_data[4],
                           'PDB':PDB_file, 'SCOP':SCOP_file, 'PFAM':PFAM_file, 'Hsapiens':Hsapiens_file, 'COG':COG_file,'VFDB':VFDB_file, 'VFDB_custom':VFDB_custom_file,'status':part.status,'is_favourite':part.is_favourite, 'created_at':part.created_at}

                output.append(one_job)
            else:
                init_data = data[0].split(" ")
                for calculation in data:

                    small_data = (calculation.split())
                    if 'SCOP' in calculation:
                        SCOP_file = small_data[4]
                    if 'PFAM' in  calculation:
                        PFAM_file = small_data[4]
                    if 'PDB' in calculation:
                        PDB_file = small_data[4]
                    if 'Hsapiens' in calculation:
                        Hsapiens_file = small_data[4]
                    if 'COG' in calculation:
                        COG_file = small_data[4]
                    if 'VFDB' in calculation:
                        VFDB_file = small_data[4]
                    if 'VFdbcustom' in calculation:
                        VFDB_custom_file = small_data[4]

                one_job = {'id': part.id, 'out_name': part.analysis_name,'insert_file':init_data[3],'profile_id':init_data[4],
                           'PDB':PDB_file, 'SCOP':SCOP_file, 'PFAM':PFAM_file, 'Hsapiens':Hsapiens_file, 'COG':COG_file,'VFDB':VFDB_file, 'VFDB_custom':VFDB_custom_file,
                           'status':part.status,'is_favourite':part.is_favourite, 'created_at':part.created_at}
                output.append(one_job)
        elif part.tool == sel_tool and sel_tool =="PCH":
            fisher, scalar,mutual,jacard = ["NO",'NO','NO','NO']
            if data[3] == "True":
                fisher = "YES"
            if data[4] == "True":
                scalar = "YES"
            if data[5] == "True":
                mutual = "YES"
            if data[6] == "True":
                jacard == "YES"
            try:
                one_job = {'id': part.id, 'out_name': part.analysis_name,'protein_id': data[2],'status': part.status,
                       'link':data[7],'fisher':fisher ,'scalar':scalar, 'mutual':mutual,'jaccard':jacard, 'is_favourite' :part.is_favourite, 'created_at':part.created_at}
            except IndexError:
                one_job = {'id': part.id, 'out_name': part.analysis_name,'protein_id': data[2],'status': part.status,
                       'link':data[6],'fisher':fisher ,'scalar':scalar, 'mutual':mutual,'jaccard':jacard, 'is_favourite' :part.is_favourite, 'created_at':part.created_at}
            output.append(one_job)




    return output
