# script to recive all genes with dufs from neighborhood
from collections import Counter
from scipy.stats import wilcoxon as test
import pandas as pd
from statsmodels.stats.multitest import multipletests as correction  #
import datetime
import json
import math
import pdb
from scipy.stats import poisson

def save_small(data):
    with open('/home/djangoadmin/final_site-project/scripts/duf_data','a+') as fs:
        fs.write(str(data))
        fs.write('\n')

class SuperSpeedAnalysisFromDomainAll:

    def __init__(self, user_pfam, user_distance, user_organism, user_cutoff, user_correction,
                 user_strand, user_output, user_level):
        """ Initializing input data
            Inputs:
                    user_pfam:  str (from pfam00000 to pfam99999)
                    user_distance: str non negative integer 1-20000
                    user_level: str
                    user_cutoff: str (none, 0, 0.00001, 0.00005, 0.0001, 0.0005, 0.001, 0.005,
                                 0.01, 0.05, 0.1, 0.2, 0.5)
                    user_correction: str (none, bonferroni, fdr_bh)
                    user_strand: str (both)
                    user_output: str
         """
        self.user_pfam = user_pfam #pfam02696
        self.user_distance = user_distance #5000
        self.user_organism = user_organism #
        self.user_cutoff = user_cutoff #none
        self.user_correction = user_correction #none
        self.user_strand = user_strand #both
        self.user_output = user_output #place
        self.user_level = user_level #order

    def open_singleline(self, path_to_file):
        """ Opens file that contain 1 column and strip it by space.
            Inputs:
                    path_to_file: str
        """

        with open(path_to_file, 'r') as f:
            file_names = [line.strip() for line in f]
        return file_names

    def open_singleline_num(self, path_to_file):
        """ Opens file that contain 1 column and strip it by space. """

        with open(path_to_file, 'r') as f:
            file_names = [float(line.strip()) for line in f]
        return file_names

    def open_multiple_line(self, path_to_file):
        """ Opens file that contains data about single genome """

        plik = []
        with open(path_to_file) as inputfile:
            for line in inputfile:
                plik.append(line.strip().split())
        return plik

    def save_data(self, file_name, message_for_output, message_for_additional_data, message_down, complete_data):
        """ Saves all stuff together as one file """

        # # LOCAL
        # with open('/mnt/d/45.76.38.24/final_site-project' + file_name + '.txt', 'w') as output_file:
        # VULTR
        # with open('/home/djangoadmin/final_site-project/media/results/test/' + file_name, 'w') as output_file:
        #     output_file.write(message_for_output)
        #     output_file.write("\n")
        #     output_file.write(message_down)
        #     output_file.write("\n")
        #     output_file.write(message_for_additional_data)
        #     output_file.write("\n")
        #     # output_file.write(complete_data)
        #     for row in complete_data.iterrows():
        #         index, data = row
        #         pre = data.tolist()
        #         output_file.write(index)
        #         output_file.write("\t")
        #         output_file.write("\t".join([str(i) for i in pre]))
        #         output_file.write("\n")

        complete_data.index.name = 'Domain'
        complete_data.to_csv('/home/djangoadmin/final_site-project' + file_name,sep='\t', mode='w' )

    def create_six_list(self, file):
        """ Open single genome file chew it and return 6 lists -> GENE, START_COORD,
        END_COORDS ,ORIENTATION, DOMAINS """

        # KLASTER
        # data = self.open_multiple_line("/home/klaster/ProFaNA v1.0/nowa_baza_danych/"+file)

        # LOCAL
        # data = self.open_multiple_line("/mnt/d/45.76.38.24/final_site-project/important_files/database_file/"+file)

        # VULTR
        data = self.open_multiple_line("/home/djangoadmin/final_site-project/important_files/nowa_baza_danych/" + file)
        start_coord = []
        end_coord = []
        orientation = []
        domains = []
        genes = []
        contig = []
        for bit in data:
            genes.append(int(bit[0]))
            start_coord.append(int(bit[1]))
            end_coord.append(int(bit[2]))
            orientation.append(bit[3])
            domains.append(bit[4])
            contig.append(bit[5])
        return genes, start_coord, end_coord, orientation, domains, contig

    def open_database(self, level, organism):

        tax = organism.replace("_", " ")
        # CHECK LEVEL AND GET LIST OF ORGANISMS
        if level == "genus":
            # VULTR
            with open('/home/djangoadmin/final_site-project/important_files/genus_level.json', "r") as handler:
                # LOCAL
                # with open('/mnt/d/45.76.38.24/final_site-project/important_files/genus_level.json', "r") as handler:
                taxonomy = json.load(handler)
        elif level == "family":
            # VULTR
            with open('/home/djangoadmin/final_site-project/important_files/family_level.json', "r") as handler:
                # LOCAL
                # with open('/mnt/d/45.76.38.24/final_site-project/important_files/family_level.json', "r") as handler:
                taxonomy = json.load(handler)
        elif level == "order":
            # VULTR
            with open('/home/djangoadmin/final_site-project/important_files/order_level.json', "r") as handler:
                # LOCAL
                # with open('/mnt/d/45.76.38.24/final_site-project/important_files/order_level.json', "r") as handler:
                taxonomy = json.load(handler)
        elif level == "class":
            # VULTR
            with open('/home/djangoadmin/final_site-project/important_files/class_level.json', "r") as handler:
                # LOCAL
                # with open('/mnt/d/45.76.38.24/final_site-project/important_files/class_level.json', "r") as handler:
                taxonomy = json.load(handler)
        elif level == "phylum":
            # VULTR
            with open('/home/djangoadmin/final_site-project/important_files/phylum_level.json', "r") as handler:
                # LOCAL
                # with open('/mnt/d/45.76.38.24/final_site-project/important_files/phylum_level.json', "r") as handler:
                taxonomy = json.load(handler)
        elif level =='all':
            with open('/home/djangoadmin/final_site-project/important_files/all_genomes', "r") as handler:
                taxonomy = [x.strip() for x in handler]
        # organism_list = taxonomy[tax]

        return taxonomy

    def genome_size_in_gene(self, genome_id, list_of_genome, list_of_size):
        """ Takes genome's id, list of genome and lists with data about how much genes in genome"""

        index_genome = list_of_genome.index(genome_id)
        genome_size_in_gene = list_of_size[index_genome]
        return genome_size_in_gene

    def size_in_genes(self, genome_or_genes):
        """ Takes list of genes and returns size of neighbourhood """

        list_of_genes = []
        for gene in genome_or_genes:
            list_of_genes.append(gene)
        return len(set(list_of_genes))

    def list_of_genes_in_neigh(self, list_of_genes, index_list):
        """ Takes overall gene's list in genome and creates complete list of genes in neighbourhood """

        list_of_genes_in_neigh = []
        for pfam_domain in index_list:
            list_of_genes_in_neigh.append(list_of_genes[pfam_domain])
        return list_of_genes_in_neigh

    def searching_for_domain_in_genome(self, pfam, start_coord, end_coord, orient, domains, contig, genes):
        """
        Takes user's pfam domain searches through lists contains coordinates,
        orientation, domains, contigs and returns complete data about all
        users' domains in genome
        """
        coords = []
        coords_counter = 0
        for domain in domains:
            one_coords_part = []
            if domain == pfam:
                orientation_pfam_domain = orient[coords_counter]
                one_coords_part.append(start_coord[coords_counter])
                one_coords_part.append(end_coord[coords_counter])
                one_coords_part.append(orientation_pfam_domain)
                one_coords_part.append(contig[coords_counter])
                one_coords_part.append(genes[coords_counter])
                coords.append(one_coords_part)
                coords_counter += 1
            else:
                coords_counter += 1
                continue
        return coords

    def presence_confirmation(self, coords, file, pfam):
        """
                Just prints out information how many pfam domains is in each genome
                during analysis propably i will have to remove it before putting it to website ...
                """

        if len(coords) == 0:
            print("In genome  " + file + " pfam domain you have been searching do not exist")
        elif len(coords) == 1:
            print("In genome  " + file + " there is " + str(len(coords)) + " " + pfam + " domain")
        else:
            print("In genome  " + file + " there are " + str(len(coords)) + " " + pfam + " domains")

    def get_range_coordinates(self, target_pfam, distance):
        """
        Based on how large neighbourhood user wants to analyze creates
        points FROM and TO, additionaly shows where main user's pfam begins and ENDS
        with orientation on strands
        """

        last_coordinate = target_pfam[1] + int(distance)
        first_coordinate = target_pfam[0] - int(distance)
        pfam_beg = target_pfam[0]
        pfam_end = target_pfam[1]
        searched_pfam_orientation = target_pfam[2]
        return last_coordinate, first_coordinate, pfam_beg, pfam_end, searched_pfam_orientation

    def get_domain_both_zeros(self, ref_gene, q_gene):

        index_list_gene = []
        counter = 0
        for gene in ref_gene:
            if gene == int(q_gene):
                index_list_gene.append(counter)
            counter += 1
        return index_list_gene

    def get_domains_both_strands(self, start_coord, end_coord, last_coordinate, first_coordinate, pfam_beg, pfam_end,
                                 contig, point):

        pfam_index_to_neigh = []
        s_coord_counter = 0
        for s_coord in start_coord:
            if s_coord <= last_coordinate and s_coord >= pfam_beg and contig[s_coord_counter] == point[
                3] and s_coord_counter not in pfam_index_to_neigh:
                pfam_index_to_neigh.append(s_coord_counter)
                s_coord_counter += 1
            else:
                s_coord_counter += 1
                continue
        e_coord_counter = 0
        for e_coord in end_coord:
            if e_coord >= first_coordinate and e_coord <= pfam_end and contig[e_coord_counter] == point[
                3] and e_coord_counter not in pfam_index_to_neigh:
                pfam_index_to_neigh.append(e_coord_counter)
                e_coord_counter += 1
            else:
                e_coord_counter += 1
                continue
        return pfam_index_to_neigh

    def collect_pfam_domain_description(self, file):
        """ Opens file only to gather information"""

        data = {}
        with open(file) as inputfile:
            for line in inputfile:
                try:

                    one_line = line.strip().split("@")
                    domena = one_line[0]
                    pre_family = one_line[1][8:]
                    delete_this = "(" + domena.upper() + ")"
                    family = pre_family.replace(delete_this, "")
                    summary = one_line[2][9:]
                    data[domena] = (family, summary)

                except IndexError:
                    continue
        return data

    def get_domains_plus_minus_strand(self, start_coord, end_coord, last_coordinate, first_coordinate,
                                      pfam_beg, pfam_end, contig, point, orientation):

        pfam_index_to_neigh_same_strand = []
        pfam_index_to_neigh_oposite_strand = []
        s_coord_counter = 0
        for s_coord in start_coord:
            if s_coord <= last_coordinate and s_coord >= pfam_beg and orientation[s_coord_counter] == point[2] and \
                    contig[s_coord_counter] == point[3] and s_coord_counter not in pfam_index_to_neigh_same_strand:
                pfam_index_to_neigh_same_strand.append(s_coord_counter)
                s_coord_counter += 1
            elif s_coord <= last_coordinate and s_coord >= pfam_beg and orientation[s_coord_counter] != point[2] and \
                    contig[s_coord_counter] == point[
                3] and s_coord_counter not in pfam_index_to_neigh_oposite_strand:
                pfam_index_to_neigh_oposite_strand.append(s_coord_counter)
                s_coord_counter += 1
            else:
                s_coord_counter += 1
                continue
        e_coord_counter = 0
        for e_coord in end_coord:
            if e_coord >= first_coordinate and e_coord <= pfam_end and orientation[e_coord_counter] == point[2] and \
                    contig[e_coord_counter] == point[3] and e_coord_counter not in pfam_index_to_neigh_same_strand:
                pfam_index_to_neigh_same_strand.append(e_coord_counter)
                e_coord_counter += 1
            elif e_coord >= first_coordinate and e_coord <= pfam_end and orientation[e_coord_counter] != point[
                2] and contig[e_coord_counter] == point[
                3] and e_coord_counter not in pfam_index_to_neigh_oposite_strand:
                pfam_index_to_neigh_oposite_strand.append(e_coord_counter)
                e_coord_counter += 1
            else:
                e_coord_counter += 1
                continue
        return pfam_index_to_neigh_same_strand, pfam_index_to_neigh_oposite_strand

    def get_list_of_domain_in_neigh(self, pfam_index, file, domains):

        to_counter = []
        party = []
        party.append(file)
        for part in pfam_index:
            party.append(domains[part])
            to_counter.append(domains[part])
        #        neighbourhood_complete.append(party)

        return to_counter

    def counting_dlok_or_dglob(self, some_counter, genome_neigh_size, mighty_domains):
        dlok_glob = []
        for domain_mighty in mighty_domains:
            if domain_mighty in some_counter.keys():
                lok_glob = some_counter.get(domain_mighty) / int(genome_neigh_size)
                dlok_glob.append(lok_glob)
            else:
                dlok_glob.append(0)
        return dlok_glob

    def count_for_dlok(self, somecounter, neigh_size, tax_name, dlok_dataframe):
        for k, v in somecounter.items():
            dlok_dataframe.at[tax_name, k] = v / neigh_size

    def multiple_test_correction(self, some_dataframe, correction_met):

        if correction_met == 'none':
            return some_dataframe
        else:
            value_to_correct = [float(x) for x in some_dataframe.PVALUE.tolist()]
            reject, pvals_corrected, alphaSidak, alphaBonf = correction(pvals=value_to_correct,
                                                                        method=correction_met,
                                                                        is_sorted=False, returnsorted=False)
            pvals = pvals_corrected.tolist()
            some_dataframe.PVALUE = pvals
            return some_dataframe

    def multiple_test_correction_list(self, some_tuple_list, correction_met):
        pvalues = [float(x) for x in some_tuple_list]

        if correction_met == 'none':
            output = pvalues

        else:
            try:

                reject, pvals_corrected, alphaSidak, alphaBonf = correction(pvals=pvalues,
                                                                            method=correction_met,
                                                                            is_sorted=False, returnsorted=False)
                pvals_after_corr = pvals_corrected.tolist()
                output = pvals_after_corr
            except ZeroDivisionError:
                output = pvalues
        return output

    def cutoff_value(self, some_dataframe, cutoff):

        domain_list = list(some_dataframe.index)
        if cutoff == 'none':
            return some_dataframe
        elif cutoff == '0':
            for i in domain_list:

                diff = float(some_dataframe.loc[i, 'Poisson_correct'])
                if diff < 0 and diff > 0:
                    some_dataframe = some_dataframe.drop([i])
            return some_dataframe
        else:
            cutoff = float(cutoff)
            for i in domain_list:
                diff = float(some_dataframe.loc[i, 'Poisson_correct'])
                if diff > cutoff:
                    some_dataframe = some_dataframe.drop([i])
            return some_dataframe

    def sort_table(self, some_dataframe):

        sorted_data = some_dataframe.sort_values('Poisson', ascending=True)
        return sorted_data

    def add_information(self, some_dataframe, dictionary):

        indeksy = list(some_dataframe.index)
        for i in indeksy:
            try:

                pfam = i[0:2] + i[4:]
                family = dictionary[pfam][0]
                summary = dictionary[pfam][1]

                some_dataframe.at[i, 'Family'] = family
                some_dataframe.at[i, 'Summary'] = summary
            except KeyError:
                continue
        return some_dataframe

    def poisson_distribution_mine(self, k, p, n):
        """
        Return value (float) of poisson distribution mine concept.
            Parameters:
                k (int): number of domain in neighborhoods
                p (float): (sum (average) of neighborhoods lenghts * sum of number of domain) / (average) sum of genome sizes
                n (int): number of neighborhoods
            How to agreggate data:
                k - Counter of all domains should be ok and easy to do
                p - sum of neighborhoods (easy += ) , sum of number of domain (z Countera), sum of genome sizes ( easy +=)
                n - number of neighborhoods (easy +=)
        """
        first_value = (n * p) ** k
        second_value = math.e ** (-1 * n * p)
        third_value = math.factorial(k)
        return (first_value * second_value) / third_value

    def poisson_distribution_KP(self, k, p, n):
        """
        Return value of poisson distribution mine concept
            Parameters:
                k (int): sum of domain occurrence in all neighborhoods
                n (int): number of neighborhoods
                p (float): ( average lengh of neighborhood * number of domain in all database) / sum of lengh of all database
            How to aggregate data:
                k Counter of all domains
                n - number of neigghborhoods (easy +=)
                p - average lengh of neighborhood ( easy +=) / number of genomes, number of domain in all ( from file), sum of lenght of all database ( const value)
        """
        first_value = (n * p) ** k
        second_value = math.e ** (-1 * n * p)
        third_value = math.factorial(k)
        return (first_value * second_value) / third_value

    def pfam_for_pf(self, dataframe):
        """Final customization of dataframe
            --> PF02696 instead of pfam02696
            --> PVALUE in 10e-3 format
            --> In what percentage % format
            --> avg occurences and density  have now 3 digits after coma
            --> drop NOam
        """

        indexy = dataframe.index
        for i in indexy:
            dataframe = dataframe.rename(index={i: i[:2].upper() + i[4:]})
        dataframe['Poisson'] = dataframe['Poisson'].map('{:.3e}'.format)
        dataframe['Poisson_correct'] = dataframe['Poisson_correct'].map('{:.3e}'.format)

        dataframe['In what percentage'] = dataframe['In what percentage'].map('{:.3%}'.format)
        dataframe['average occurence in neighborhood'] = dataframe['average occurence in neighborhood'].map(
            '{:.3}'.format)
        dataframe['average occurence in genome'] = dataframe['average occurence in genome'].map('{:.3}'.format)

        # dataframe = dataframe.round({"average occurence in neighborhood": 3,"average occurence in genome": 3,
        #                              "Density difference": 3})
        if "NOam" in indexy:
            dataframe = dataframe.drop(index="NOam")
        # dataframe = dataframe.style.format({'In what percentage': '{:,.3%}'.format})
        dataframe.drop('average occurence in neighborhood', axis=1, inplace=True)
        dataframe.drop('average occurence in genome', axis=1, inplace=True)
        dataframe.drop('avg neigh size', axis=1, inplace=True)
        dataframe.drop('domain in whole database', axis=1, inplace=True)
        if self.user_correction == 'none':
            dataframe.drop('Poisson_correct', axis=1, inplace=True)
        else:
            dataframe.drop('Poisson', axis=1, inplace=True)

        return dataframe

    def poisson_distribution(self, p, n, domain, percentage_counter):

        if int(percentage_counter[domain]) < p * n:
            return 1
        else:
            sums = 0
            k = 0
            number_of_samples = int(percentage_counter[domain])
            while k < number_of_samples:
                sums += poisson.pmf(k=k, mu=n * p)
                k += 1
            if sums >= 1:
                return float(0.0)
            else:
                return 1 - sums

    def save_small(self,data):
        with open('/home/djangoadmin/final_site-project/scripts/duf_data','a+') as fs:
            fs.write(str(data))
            fs.write('\n')

    def go(self):

        genome_id_size_in_gene = self.open_multiple_line(
            "/home/djangoadmin/final_site-project/important_files/GENOME_ID_SIZE_IN_GENE.txt")
        with open('/home/djangoadmin/final_site-project/important_files/counter_domains', 'r') as handler:
            pfam_counter_domains = json.load(handler)


        genome_id = [x[0] for x in genome_id_size_in_gene]
        size_in_gene = [x[1] for x in genome_id_size_in_gene]

        genome_number_overall = 0
        genome_number_to_stat = 0

        level = self.open_database(self.user_level, self.user_organism)

        just_neigh_data = []
        genomes_with_domain = []
        neigh_genome_size = []
        counter = 0
        counter_db = 0
        sum_of_genes_in_neighborhoods = 0
        number_of_neighborhoods = 0
        percentage_counter = Counter()

        # Dlok operations
        for file in level:
            # print(file)
            counter_db += 1
            counter += 1
            try:
                genes, start_coord, end_coord, orientation, domains, contig = self.create_six_list(file)
            except FileNotFoundError:
                continue
            except ValueError:
                continue
            file_name_raw = file.split('/')
            tax_name = file_name_raw[-1]
            genome_number_overall += 1
            number_of_genes_in_genome = self.genome_size_in_gene(tax_name, genome_id, size_in_gene)
            coords = self.searching_for_domain_in_genome(self.user_pfam, start_coord, end_coord, orientation,
                                                            domains, contig, genes)
            if len(coords) > 0:
                genome_number_to_stat += 1

            for point in coords:
                number_of_neighborhoods += 1

                if self.user_distance != 0:
                    last_coordinate, first_coordinate, pfam_beg, pfam_end, searched_pfam_orientation = \
                        self.get_range_coordinates(point, self.user_distance)
                    pfam_index_to_neigh = self.get_domains_both_strands(start_coord, end_coord, last_coordinate,
                                                                        first_coordinate, pfam_beg, pfam_end,
                                                                        contig,
                                                                        point)
                else:
                    pfam_index_to_neigh = self.get_domain_both_zeros(genes, point[4])

                genes_in_neigh = self.list_of_genes_in_neigh(genes, pfam_index_to_neigh)
                number_of_genes_in_neigh = self.size_in_genes(genes_in_neigh)
                whole = self.get_list_of_domain_in_neigh(pfam_index_to_neigh, file, domains)
                percentage_counter += Counter(set(whole))
                just_neigh_data.append(whole)
                genomes_with_domain.append(file)
                neigh_genome_size.append((number_of_genes_in_neigh, number_of_genes_in_genome))
                sum_of_genes_in_neighborhoods += int(number_of_genes_in_neigh)
                # pdb.set_trace()
        alls = []
        neigh_counter = Counter()

        for i in just_neigh_data:
            for j in i:
                if j not in alls:
                    alls.append(j)
        genome_counter = Counter()

        counter = 0
        for domain_list, genome_in_domain, ng_size in zip(just_neigh_data, genomes_with_domain, neigh_genome_size):
            counter += 1
            genes, start_coord, end_coord, orientation, domains, contig = self.create_six_list(genome_in_domain)
            genome_domains_counter = Counter(domains)
            genome_counter += genome_domains_counter
            neigh_domains_counter = Counter(domain_list)
            neigh_counter += neigh_domains_counter


        najlepsze_dane = pd.DataFrame(columns=['occurence in neighborhoods',
                                                'average occurence in neighborhood', 'occurence genomes',
                                                'average occurence in genome',
                                                'In what percentage', 'Poisson', 'Poisson_correct', 'P', 'N', 'K',
                                                'avg neigh size', 'domain in whole database',
                                                'Family', 'Summary', ])

        scores_poisson = []

        counter = 0
        for i in alls:
            counter += 1

            p_KP = ((sum_of_genes_in_neighborhoods / genome_number_to_stat) * pfam_counter_domains[i]) / 326628067
            najlepsze_dane.at[i, 'P'] = p_KP
            najlepsze_dane.at[i, 'avg neigh size'] = sum_of_genes_in_neighborhoods / genome_number_to_stat
            najlepsze_dane.at[i, 'domain in whole database'] = pfam_counter_domains[i]
            # najlepsze_dane.at[i,'sum of database len'] = 326628067
            poisson_test_result = self.poisson_distribution(p_KP, number_of_neighborhoods, i, percentage_counter)
            najlepsze_dane.at[i, 'K'] = int(percentage_counter[i])
            najlepsze_dane.at[i, 'N'] = number_of_neighborhoods
            najlepsze_dane.at[i, 'P'] = p_KP
            najlepsze_dane.at[i, 'Poisson'] = poisson_test_result
            scores_poisson.append(poisson_test_result)

        poisson_after_correction = self.multiple_test_correction_list(scores_poisson, self.user_correction)

        for domena, poisson_corr, in zip(alls, poisson_after_correction):
            najlepsze_dane.at[domena, 'Poisson_correct'] = poisson_corr
            najlepsze_dane.at[domena, 'In what percentage'] = percentage_counter[domena] / len(just_neigh_data)
            najlepsze_dane.at[domena, 'occurence in neighborhoods'] = neigh_counter[domena]
            najlepsze_dane.at[domena, 'average occurence in neighborhood'] = neigh_counter[domena] / len(
                just_neigh_data)
            najlepsze_dane.at[domena, 'occurence genomes'] = genome_counter[domena]
            najlepsze_dane.at[domena, 'average occurence in genome'] = genome_counter[domena] / len(
                set(genomes_with_domain))

        after_cutoff = self.cutoff_value(najlepsze_dane, self.user_cutoff)
        self.save_small(str(len(after_cutoff.index)-1))
        print(self.user_pfam, len(after_cutoff.index)-1, najlepsze_dane['In what percentage'].max(),najlepsze_dane['In what percentage'].min() )




with open('/home/djangoadmin/final_site-project/important_files/DUFs_to_calculate','r') as handler:
    dufs = [x.strip() for x in handler]

dufs_cor = ['pfam'+ x[2:] for x in dufs]

for i in dufs_cor:
    a = SuperSpeedAnalysisFromDomainAll(i, 5000, 'all', '0.00001' ,'bonferroni', 'both','/media/results/all_db_q.txt', 'all')
    a.go()


# f = open('/home/djangoadmin/final_site-project/important_files/counter_domains')
# domains_to_analysis = json.load(f)
# results = {}
#
# for k,v in domains_to_analysis.items():
#     print(k,v)
#     if k != 'noPfam':
#         save_small(k)
#         save_small(v)
#         pfam_domain = k
#         a = SuperSpeedAnalysisFromDomainAll(pfam_domain, 5000, 'all', '0.00001' ,'bonferroni', 'both','/media/results/all_db_q.txt', 'all')
#         a.go()



# for i in dufs_cor:
#     try:
#         results[i] = counter[i]
#     except KeyError:
#         continue
# new_results= sorted(results.items(), key=lambda x: x[1], reverse=True)
# pdb.set_trace()




# for k in new_results[:100]:
#     print(k)
#     pfam_domain = k[0]
#     #pdb.set_trace()
#     a = SuperSpeedAnalysisFromDomainAll(pfam_domain, 5000, 'all', '0.00001' ,'bonferroni', 'both','/media/results/all_db_q.txt', 'all')
#     a.go()
