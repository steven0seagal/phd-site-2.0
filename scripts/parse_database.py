from collections import Counter
import json

with open("/home/djangoadmin/final_site-project/important_files/all_genomes",'r') as handler:
    data = [x.strip() for x in handler]

dictionary = Counter()
counter = 0
for genome in data:
    print(counter , end='\r')
    counter += 1
    try:

        with open("/home/djangoadmin/final_site-project/important_files/nowa_baza_danych/{}".format(genome),'r') as handler:
            genome = [x.strip().split() for x in handler]

        pfams = [x[4] for x in genome if len(x) > 2 ]
        small_counter = Counter(pfams)
        dictionary += small_counter
    except FileNotFoundError:
        continue
# import pdb; pdb.set_trace()
with open("/home/djangoadmin/final_site-project/important_files/counter_doamins",'w') as handler:
    json.dump(dictionary,handler)
