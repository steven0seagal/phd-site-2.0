from kintaro.models import *


# with open('mediafiles/kintaro/align_seq','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation[1:]:
#     print(line)
#     family_name = line[1]
#     file_name = line[2]
#     family_id = line[0]
#     file = f'media/kintaro/align/{file_name}'
#     family = Family.objects.get(family_id = family_id)
#     MatchingSeq(family = family, family_name=family_name, file = file).save()


# with open('mediafiles/kintaro/domain_seq','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation[1:]:
#     print(line)
#     family_name = line[1]
#     file_name = line[2]
#     family_id = line[0]
#     file = f'media/kintaro/domain/{file_name}'
#     family = Family.objects.get(family_id = family_id)
#     DomainSeq(family = family, family_name=family_name, file = file).save()


# with open('mediafiles/kintaro/fullseq','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation[1:]:
#     print(line)
#     family_name = line[1]
#     file_name = line[3]
#     family_id = line[0]
#     file = f'media/kintaro/full/{file_name}'
#     family = Family.objects.get(family_id = family_id)
#     FullSeq(family = family, family_name=family_name, file = file).save()


# with open('mediafiles/kintaro/hmm_seq','r') as handler:
#    relation = [x.strip().split() for x in handler]
# for line in relation[1:]:
#    print(line)
#    family_name = line[1]
#    file_name = line[2]
#    family_id = line[0]
#    file = f'media/kintaro/hmm/{file_name}'
#    family = Family.objects.get(family_id = family_id)
#    HMMmodels(family = family, family_name=family_name, hmm_file = file,hmm_family_id=family_id).save()

# with open('mediafiles/kintaro/web','r') as handler:
#    relation = [x.strip().split() for x in handler]
# for line in relation[1:]:
#    print(line)
#    family_name = line[1]
#    file_name = line[2]
#    family_id = line[0]
#    file = f'media/kintaro/weblogo/{file_name}'
#    family = Family.objects.get(family_id = family_id)
#    WebLogo(family = family, file = file).save()

#PFAM
# https://www.ebi.ac.uk/interpro/entry/pfam/PF14487/
#with open('mediafiles/kintaro/external','r') as handler:
#   relation = [x.strip().split() for x in handler]

#for line in relation[1:]:
#   family_id = line[0]
#   pfam_id = line[2]
#   ipr = line[3]
#   family = Family.objects.get(family_id = family_id)
#   ExternalDB(family_id=family.id, source = "Pfam", site='https://www.ebi.ac.uk/interpro/entry/pfam/'+pfam_id,db_id=pfam_id).save()
#   ExternalDB(family_id=family.id, source = "InterPro", site='https://www.ebi.ac.uk/interpro/entry/InterPro/'+ipr,db_id=ipr).save()
#INTERPRO
# https://www.ebi.ac.uk/interpro/entry/InterPro/IPR004166/

with open('mediafiles/kintaro/models_seq','r') as handler:
   relation = [x.strip().split() for x in handler]

for line in relation[1:]:
    family_id = line[0]
    file = f'mediafiles/kintaro/models/{line[2]}'
    plddt = line[3]
    family = Family.objects.get(family_id = family_id)
    FoldedModel(family_id = family.id, file = file, plddt = plddt).save()

with open('mediafiles/kintaro/structures_seq','r') as handler:
    relation = [x.strip().split() for x in handler]

for line in relation[1:]:
    family_id = line[0]
    file = f'mediafiles/kintaro/structures/{line[2]}'
    
    family = Family.objects.get(family_id = family_id)
    Structure(family_id = family.id, file = file).save()


#HMM results 

# data = Family.objects.all()
# for i in data:
#     ResultsHmm(file_name = f'{i.family_id}.hmm', hmm_name = i.family_id, family_id = i.family_id, family_name_in_results = i.family_name).save()