from art.models import *

families = Family.objects.all()

# for f in families:
#     with open(f'mediafiles/art/descriptions_family/description_{f.family_id}.txt','r') as handler:
#         description = handler.read()
#     with open(f'mediafiles/art/origin/origin_{f.family_id}.txt', 'r') as handler:
#         origin = handler.read()
#
#     f.description = description
#     f.certificate = origin
#     f.save()

# with open('mediafiles/art/family_clade_relation','r') as handler:
#     relation = [x.strip().split() for x in handler]
#
# for line in relation:
#     print(line)
#     if line[0] != 'none':
#         clade_id = line[0]
#         family_id = line[2]
#         family = Family.objects.get(family_id = family_id)
#         _clade = Clade.objects.get(clade_id = clade_id)
#
#         family.clade.add(_clade)
#

#
# with open('mediafiles/art/aligned_seq','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation:
#     print(line)
#     if line[2] != 'none':
#         family_id = line[0]
#         family_name = line[1]
#         file = f'media/art/align/{family_id}_align.fasta'
#         family = Family.objects.get(family_id = family_id)
#         MatchingSeq(family = family, family_name=family_name, file = file).save()
#
#
# with open('mediafiles/art/domain_seq','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation:
#     print(line)
#     family_name= line[0]
#     family = Family.objects.get(family_name = family_name)
#
#     family_id = family.family_id
#     file = f'media/art/domain/{family_id}_domains.fasta'
#     family = Family.objects.get(family_id = family_id)
#     DomainSeq(family = family, family_name=family_name, file = file).save()

# with open('mediafiles/art/full_seq','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation:
#     print(line)
#     family_id = line[0]
#     family_name= line[1]
#     family = Family.objects.get(family_name = family_name)
#
#     family_id = family.family_id
#     file = f'media/art/full/{family_id}_full.fasta'
#     family = Family.objects.get(family_id = family_id)
#     FullSeq(family = family, family_name=family_name, file = file).save()
#

# with open('mediafiles/art/hmm_file','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation:
#     print(line)
#     family_name= line[0]
#     family = Family.objects.get(family_name = family_name)
#
#     family_id = family.family_id
#     file = f'media/art/hmm/{family_id}_HMM.hmm'
#     family = Family.objects.get(family_id = family_id)
#     HMMmodels(family = family, family_name=family_name, hmm_file = file, hmm_family_id=family_id).save()

# with open('mediafiles/art/interpro_data','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation:
#     print(line)
#     family_id= line[2]
#
#     with open(f'mediafiles/art/interpro/{line[0]}_abstract.txt','r') as handler:
#         data = handler.read()
#
#     family = Family.objects.get(family_id = family_id)
#     InterPro(family = family, inter_id=line[0], abstract=data).save()
#
# with open('mediafiles/art/citation','r') as handler:
#     relation = [x.strip().split() for x in handler]
# for line in relation:
#     print(line)
#     family_id= line[1]
#
#
#     family = Family.objects.get(family_id = family_id)
#     Citation(family = family, pmid=line[0]).save()


#

#
#
# with open('important_files/art/family','r') as handler:
#     data = [x.strip().split('\t') for x in handler]
#
# for line in data[1:]:
#     print(line)
#     public_line = line[4]
#     if public_line == 'T':
#         public = True
#     elif public_line == 'F':
#         public = False
#     Family(family_id=line[0], family_name=line[1], public=public).save()

# with open('important_files/art/keywords','r') as handler:
#     data = [x.strip().split('\t') for x in handler]
# for line in data:
#     print(line)
#     family = Family.objects.get(family_id=line[0])
#     keyword = KeywordFamily.objects.get(keyword=line[1])
#     keyword.family.add(family)

# with open('mediafiles/art/pdb_data','r') as handler:
    # data =[x.strip().split('\t') for x in handler]
# for line in data:
    # print(line)
    # family= Family.objects.get(family_id=line[0])
    # from scripts imStructure(family=family,pdb_id=line[2], protein_name=line[2],pdb_file='none',merged_file='none').save()
