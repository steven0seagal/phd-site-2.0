from art.models import Citation
from Bio import Entrez,SeqIO


citations = Citation.objects.all()

for citation in citations:
    try:
        Entrez.email = 'bartosz.piotr.baranowski@gmail.com'
        handle = Entrez.efetch(db="pubmed", id=citation.pmid, rettype="gb", retmode="text")
        record = SeqIO.read(handle, "genbank")
        handle.close()
        journal = record.annotations['references'][0].journal
        title = record.annotations['references'][0].title
        authors = record.annotations['references'][0].authors
        Citation.objects.filter(pmid = citation.pmid).update(title=title,author=authors, sci_mag=journal)
    except:
        continue

