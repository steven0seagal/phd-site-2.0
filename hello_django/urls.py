from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from upload.views import image_upload

urlpatterns = [
    path('', include('mainsite.urls')),
    # path("", image_upload, name="upload"),
    path("admin/", admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('alignments_tools/', include('alignments_tools.urls')),
    path('neighborhood_analysis/', include('neighborhood_analysis.urls')),
    path('news/', include('news.urls')),
    path('searchdb_coocurence/', include('searchdb_coocurence.urls')),
    path('hmmer_fixer/', include('hmmer_fixer.urls')),
    path('ffas/', include('ffas.urls')),
    path('accounts2/', include('allauth.urls')),
    path('art/', include('art.urls')),
    path('kintaro/',include('kintaro.urls')),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
