from django.db import models

# Create your models here.
class NeighAnalyzDatabase(models.Model):

    #USER
    user_id = models.IntegerField()

    #INPUT
    analysis_type = models.CharField(max_length = 200)
    input = models.CharField(max_length = 128)
    blast_cutoff = models.CharField(max_length = 16,blank=True)
    neigh_size = models.IntegerField()
    test_correction = models.CharField(max_length = 200)
    strand = models.CharField(max_length = 200)
    cut_off = models.CharField(max_length = 9)
    analysis_name = models.CharField(blank=True, max_length = 200)
    database = models.CharField(max_length = 200)
    average_level = models.CharField(max_length = 200, blank=True)

    #RESULTS
    ontology_results = models.CharField(max_length = 200, blank=True)
    output_results = models.CharField(max_length = 200, blank=True)

    #BOOLEAN
    status = models.CharField(max_length = 200, default='Queue')
    is_favorite = models.BooleanField(default=False)
    is_uploaded = models.BooleanField(default=False)
    is_average = models.BooleanField(default=False)

    #TIMESTAMP
    updated_time = models.DateTimeField(auto_now=True,blank=True)
    created_time = models.DateTimeField(auto_now_add=True,blank=True)

    def __str__(self):
        return self.analysis_name

class NeighAnalyzSequenceDatabase(models.Model):

    #USER
    user_id = models.IntegerField()

    #INPUT
    sequence = models.TextField()
    sequence_title = models.CharField(max_length =256)
    analysis_name = models.CharField(max_length =256)
    blast_cutoff = models.CharField(max_length = 16,blank=True)
    neigh_size = models.IntegerField()
    test_correction = models.CharField(max_length = 200)
    strand = models.CharField(max_length = 200)
    cut_off = models.CharField(max_length = 9)
    database = models.CharField(max_length = 200, blank=True, default='alldatabase')

    #RESULTS
    status = models.CharField(max_length = 200, default='Queue')

    ontology_results = models.CharField(max_length = 200, blank=True)
    output_results = models.CharField(max_length = 200, blank=True)

    #BOOLEAN
    is_favorite = models.BooleanField(default=False)
    is_uploaded = models.BooleanField(default=False)

    #TIMESTAMP
    updated_time = models.DateTimeField(auto_now=True,blank=True)
    created_time = models.DateTimeField(auto_now_add=True,blank=True)


    def __str__(self):
        return self.analysis_name
