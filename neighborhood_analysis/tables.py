import django_tables2 as tables
from accounts.models import CompleteQueue

class ProductHTMxTable(tables.Table):
    class Meta:
        model = CompleteQueue
        template_name = "tutorial/bootstrap_htmx.html"
