class QueryParamsValidator:
    def __init__(self, query_params):
        self.query_params = query_params

    def get_query_params_dict(self, request):
        return dict((v, request.GET.get(f"{v}")) for v in self.query_params)

    def validate(self, request):
        filtered_errors = list(
            filter(lambda param: request.GET.get(param) is None, self.query_params)
        )
        if filtered_errors:
            return filtered_errors
        else:
            return self.get_query_params_dict(request)
