from django.urls import path
from . import views

urlpatterns = [
    path('', views.neighana_menu, name = 'neighana_menu'),

    path('pfam_domain',views.neighana, name = 'neighana'),
    path('gene', views.geneana, name = 'geneana'),
    path('pfam_domain_fam', views.neighana_fam, name='neighana_fam'),
    path('pfam_domain_all', views.neighana_all, name='neighana_all'),
    path('sequence',views.neighana_seq, name='neighana_seq'),

    path('count_from_domain', views.count_from_domain, name = 'count_from_domain'),
    path('count_from_gene', views.count_from_gene, name='count_from_gene'),
    path('count_from_domain_family', views.count_from_domain_family, name='count_from_domain_family'),
    path('count_from_domain_all', views.count_from_domain_all, name='count_from_domain_all'),
    path('count_from_sequence',views.count_from_sequence, name='count_from_sequence'),

    # path("people/", views.JobListView.as_view()),
    # path("pp2/", views.ProductHTMxTableView.as_view(),name='product_htmx'),
    path('files/<str:filename>/', views.get_file),
    path('files/ontology/<str:filename>/',views.get_file_ontology),
    path('files/taxonomy/<str:filename>/',views.get_file_taxonomy),
    path('statistics/',views.statistics, name='statistics'),


    #API
    path('api/get_neigh_sequence/', views.GetNeighSeqForAnalysis.as_view(), name="neigh_seq"),
    path('api/send_neigh_sequence/', views.UploadNeighSeqResults.as_view(), name="send_neigh_seq"),
    path('api/update_neigh_sequence/', views.UpdateNeighSeqStatus.as_view(), name="update_neigh_seq"),

    path('api/get_neigh_anal/', views.GetNeighForAnalysis.as_view(), name="neigh_anal"),
    path('api/send_neigh', views.UploadNeighSeqResults.as_view(), name="send_neigh"),
    path('api/update_neigh/', views.UpdateNeighStatus.as_view(),name="update_neigh")
]
