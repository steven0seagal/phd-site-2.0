# products/filters.py
from decimal import Decimal
from django.db.models import Q
import django_filters
from accounts.models import CompleteQueue

class ProductFilter(django_filters.FilterSet):
    query = django_filters.CharFilter(method='universal_search',
                                      label="")

    class Meta:
        model = CompleteQueue
        fields = ['tool']

    def universal_search(self, queryset, name, value):
        if value.replace(".", "", 1).isdigit():
            value = Decimal(value)
            return CompleteQueue.objects.filter(
                Q(price=value) | Q(cost=value)
            )

        return CompleteQueue.objects.filter(
            Q(name__icontains=value) | Q(category__icontains=value)
        )
