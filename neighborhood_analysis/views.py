import csv
import json
import random
import string
import uuid

from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import CompleteQueue
from scripts.PfamValidate import PfamValidator
from scripts.pfam_input import retype_domain, check_if_domain_can_be_reachable
from .models import NeighAnalyzSequenceDatabase
from .validators import QueryParamsValidator


def neighana_menu(request):
    return (render(request, 'tools/input/neighborhood_analyzer_menu.html'))


def statistics(request):
    with open("/usr/src/app/neighborhood_analysis/data/new_counter_2023.json", "r") as f:
        data = json.load(f)
    new_dict = {}
    for key in data:
        new_dict[key.replace('pfam', 'PF')] = data[key]
    # add dict to pandas 

    return (render(request, 'tools/input/neighborhood_analyzer_statistics.html', {'data': new_dict}))


def geneana(request):
    my_data = [1, 2, 3, 4, 5, 6, 7]
    context = {'my_data': my_data}
    return (render(request, 'tools/input/neighborhood_analyzer_gene.html', context))


def neighana(request):
    return render(request, 'tools/input/neighborhood_analyzer_domain.html')


def neighana_fam(request):
    return render(request, 'tools/input/neighborhood_analyzer_domain_fam.html')


def neighana_all(request):
    return render(request, 'tools/input/neighborhood_analyzer_domain_all.html')


def neighana_seq(request):
    my_data = """
        MAVYRAALGASLAAARLLPLGRCSPSPAPRSTLSGAAMEPAPRWLAGLRFDNRALRALPV
        EAPPPGPEGAPSAPRPVPGACFTRVQPTPLRQPRLVALSEPALALLGLGAPPAREAEAEA
        ALFFSGNALLPGAEPAAHCYCGHQFGQFAGQLGDGAAMYLGEVCTATGERWELQLKGAGP
        TPFSRQADGRKVLRSSIREFLCSEAMFHLGVPTTRAGACVTSESTVVRDVFYDGNPKYEQ
        CTVVLRVASTFIRFGSFEIFKSADEHTGRAGPSVGRNDIRVQLLDYVISSFYPEIQAAHA
        SDSVQRNAAFFREVTRRTARMVAEWQCVGFCHGVLNTDNMSILGLTIDYGPFGFLDRYDP
        DHVCNASDNTGRYAYSKQPEVCRWNLRKLAEALQPELPLELGEAILAEEFDAEFQRHYLQ
        KMRRKLGLVQVELEEDGALVSKLLETMHLTGADFTNTFYLLSSFPVELESPGLAEFLARL
        MEQCASLEELRLAFRPQMDPRQLSMMLMLAQSNPQLFALMGTRAGIARELERVEQQSRLE
        QLSAAELQSRNQGHWADWLQAYRARLDKDLEGAGDAAAWQAEHVRVMHANNPKYVLRNYI
        AQNAIEAAERGDFSEVRRVLKLLETPYHCEAGAATDAEATEADGADGRQRSYSSKPPLWA
        AELCVTUSS"""
    context = {'my_data': my_data}
    return render(request, 'tools/input/neighborhood_analyzeer_sequence.html', context)


def count_from_sequence(request):
    print(request.POST['sequence']
          )
    sequence = request.POST['sequence']
    analysis_name = request.POST['out_name']
    user_id = request.user.id
    blast_cutoff = request.POST['blast_cutoff']
    neigh_size = request.POST['range_search']
    test_correction = request.POST['test_correction']
    strand = request.POST['strand_select']
    cut_off = request.POST['cut_off']
    database = request.POST['database']
    if sequence[0] == '>':
        sequence_title = sequence.split('\n')[0]
        sequence = sequence.split('\n')[1:]

        sequence = ''.join([line.strip() for line in sequence])
    else:
        sequence_title = '>None sequence description'
        sequence = ''.join([line.strip() for line in sequence])
    out_name = uuid.uuid4()
    uniqe_id = f"/media/results/{out_name}.txt"
    ontology = f"/media/results/{out_name}_ontology.txt"
    NeighAnalyzSequenceDatabase.objects.create(user_id=user_id,
                                               sequence=sequence,
                                               sequence_title=sequence_title,
                                               analysis_name=analysis_name,
                                               output_results=uniqe_id,
                                               ontology_results=ontology,
                                               blast_cutoff=blast_cutoff,
                                               neigh_size=neigh_size,
                                               test_correction=test_correction,
                                               strand=strand,
                                               cut_off=cut_off, database=database)

    return redirect('dashboard')


# NA
def count_from_domain(request):
    #    fulltext = request.GET['fulltext']
    pfam_domain = request.GET['pfam_domain']
    range_search = request.GET['range_search']
    cut_off = request.GET['cut_off']
    database_taxa = request.GET['database_taxa']
    strand_select = request.GET['strand_select']
    out_name = request.GET['out_name']
    test_correction = request.GET['test_correction']
    # out_time = '12:34:56.789012'
    user_id = request.user.id
    skip_negative = "yes"

    pfam = PfamValidator(pfam_domain)
    pfam_value = pfam.Validate()
    domain_for_database, domain_to_search = retype_domain(pfam_domain)
    database = request.GET['database']
    if pfam_value is True:
        if check_if_domain_can_be_reachable(database_taxa, domain_to_search) is True:
            letters = string.ascii_lowercase
            end_end = ''.join(random.choice(letters) for i in range(15))
            link_down = '/media/results/' + end_end + '.txt'
            ontology = '/media/results/' + end_end + '_ontology.txt'

            ready_script = 'python3 /usr/src/app/scripts/execute_order_66.py ' \
                           '{} {} {} {} {} {} {} {} {}'.format(domain_to_search, range_search, database_taxa, cut_off,
                                                               test_correction, strand_select, link_down, skip_negative,
                                                               database)

            job = CompleteQueue(user_id=user_id, tool='NA', status='Queue', analysis_name=out_name,
                                script=ready_script, file=link_down, ontology_file=ontology)
            job.save()

            return redirect('dashboard')
        else:
            return render(request, 'tools/error/heavy_calculation.html')
    else:
        return render(request, 'tools/error/wrong_pfam.html')


# NAF
def count_from_domain_family(request):
    pfam_domain = request.GET['pfam_domain']
    range_search = request.GET['range_search']
    cut_off = request.GET['cut_off']
    database_taxa = request.GET['database_taxa']
    strand_select = request.GET['strand_select']
    out_name = request.GET['out_name']
    test_correction = request.GET['test_correction']
    user_id = request.user.id
    database = request.GET['database']
    skip_negative = "no"
    pfam = PfamValidator(pfam_domain)
    pfam_value = pfam.Validate()
    domain_for_database, domain_to_search = retype_domain(pfam_domain)

    letters = string.ascii_lowercase
    end_end = ''.join(random.choice(letters) for i in range(15))

    link_down = '/media/results/' + end_end + '.txt'
    ontology = '/media/results/' + end_end + '_ontology.txt'
    with open('/usr/src/app/important_files/database.json', "r") as handler:

        taxonomy = json.load(handler)

    if pfam_value == True:

        ready_script = f"python3 /usr/src/app/scripts/execute_order_family.py {domain_to_search} {range_search} {database_taxa} {cut_off} {test_correction} {strand_select} {link_down} {database}"

        job = CompleteQueue(user_id=user_id, tool='NAF', status='Queue', analysis_name=out_name,
                            script=ready_script, file=link_down, ontology_file=ontology)
        job.save()

        return redirect('dashboard')

    else:
        return render(request, 'tools/error/wrong_pfam.html')


# NAG
def count_from_gene(request):
    # file upload
    if request.method == 'POST':
        context = {}
        uploaded_file = request.FILES['gene_list']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        context['url'] = fs.url(name)
        link_do_pliku = context['url']

    range_search = request.POST['range_search']
    cut_off = request.POST['cut_off']
    database = request.POST['database']
    strand_select = request.POST['strand_select']
    out_name = request.POST['out_name']
    test_correction = request.POST['test_correction']
    user_id = request.user.id

    letters = string.ascii_lowercase
    end_end = ''.join(random.choice(letters) for i in range(15))
    link_down = '/media/results/' + end_end + '.txt'
    ontology = '/media/results/' + end_end + '_ontology.txt'
    ready_script = 'python3 /usr/src/app/scripts/execute_order_99.py ' \
                   '{} {} {} {} {} {} {} {}'.format(link_do_pliku, range_search, "Whole_database", cut_off,
                                                    test_correction, strand_select, link_down, database)
    job = CompleteQueue(user_id=user_id, tool='NAG', status='Queue', analysis_name=out_name,
                        script=ready_script, file=link_down, ontology_file=ontology)
    job.save()

    return redirect('dashboard')


# NAA
def count_from_domain_all(request):
    pfam_domain = request.GET['pfam_domain']
    range_search = request.GET['range_search']
    cut_off = request.GET['cut_off']
    strand_select = request.GET['strand_select']
    out_name = request.GET['out_name']
    test_correction = request.GET['test_correction']
    user_id = request.user.id
    method_average = request.GET["method_average"]
    database = request.GET['database']
    if method_average == "no":
        database_taxa = "all_genomes"
        skip_negative = "yes"
        pfam = PfamValidator(pfam_domain)
        pfam_value = pfam.Validate()
        domain_for_database, domain_to_search = retype_domain(pfam_domain)

        if pfam_value is True:

            if check_if_domain_can_be_reachable(database_taxa, domain_to_search) is True:
                letters = string.ascii_lowercase
                end_end = ''.join(random.choice(letters) for i in range(15))
                link_down = '/media/results/' + end_end + '.txt'
                file_place = end_end + '.txt'
                ontology = '/media/results/' + end_end + '_ontology.txt'
                ready_script = 'python3 /usr/src/app/scripts/execute_order_66.py ' \
                               '{} {} {} {} {} {} {} {} {}'.format(domain_to_search, range_search, database_taxa,
                                                                   cut_off,
                                                                   test_correction, strand_select, file_place,
                                                                   skip_negative, database)

                job = CompleteQueue(user_id=user_id, tool='NAD', status='Queue', analysis_name=out_name,
                                    script=ready_script, file=link_down, ontology_file=ontology)
                job.save()
                return redirect('dashboard')
            else:
                return render(request, 'tools/error/heavy_calculation.html')
        else:
            return render(request, 'tools/error/wrong_pfam.html')



    elif method_average != "no":
        database_taxa = "all_genomes"
        pfam = PfamValidator(pfam_domain)
        pfam_value = pfam.Validate()
        domain_for_database, domain_to_search = retype_domain(pfam_domain)

        if pfam_value is True:

            if check_if_domain_can_be_reachable(database_taxa, domain_to_search) is True:
                letters = string.ascii_lowercase
                end_end = ''.join(random.choice(letters) for i in range(15))
                link_down = '/media/results/' + end_end + '.txt'
                file_place = end_end + '.txt'
                ontology = '/media/results/' + end_end + '_ontology.txt'

                if method_average == "genus":

                    with open('/usr/src/app/important_files/genus_level.json', "r") as handler:
                        taxonomy = json.load(handler)

                elif method_average == "family":
                    with open('/usr/src/app/important_files/family_level.json', "r") as handler:
                        taxonomy = json.load(handler)

                elif method_average == "order":
                    with open('/usr/src/app/important_files/order_level.json', "r") as handler:
                        taxonomy = json.load(handler)

                elif method_average == "class":
                    with open('/usr/src/app/important_files/class_level.json', "r") as handler:
                        taxonomy = json.load(handler)

                elif method_average == "phylum":
                    with open('/usr/src/app/important_files/phylum_level.json', "r") as handler:
                        taxonomy = json.load(handler)
                genus_database = list(taxonomy.keys())

                ready_script = ""

                files_for_analysis = []

                for genus in genus_database:
                    new_tax = "_".join(genus.split(' '))
                    files_for_analysis.append(new_tax)
                ready_script += 'python3 /usr/src/app' \
                                '/scripts/execute_order_all_avg.py {} {} {} {} {} {} {} {} {}'.format(
                    domain_to_search, range_search, 'none', 'none', test_correction, strand_select,
                    file_place, method_average, database)

                job = CompleteQueue(user_id=user_id, tool='NAD', status='Queue', analysis_name=out_name,
                                    script=ready_script, file=link_down, ontology_file=ontology)
                job.save()

                return redirect('dashboard')

            else:
                return render(request, 'tools/error/heavy_calculation.html')

        return redirect("dashboard")


def tab_file_to_dict(filename):
    delimiter = "\t"
    data = []
    try:
        with open(f"/usr/src/app/mediafiles/results/{filename}.txt", "r") as f:
            reader = csv.reader(f, delimiter=delimiter)

            header = next(reader)
            for row in reader:
                d = {}
                for i, value in enumerate(row):
                    key = header[i].replace(' ', '_').replace(',', '')
                    d[key] = value
                data.append(d)

        return data
    except:
        return "No data found"


def all_data(filename):
    go_data = parse_go_data(f"{filename}_ontology")
    results = tab_file_to_dict(filename)
    test = 'percentage_of_analyzed_groups' in results[0].keys()
    # biological_process_dict,molecular_function_dict, cellular_component_dict = parse_go_data('taxonomy/'+filename+'_taxonomy')
    # print(biological_process_dict,molecular_function_dict, cellular_component_dict,results)
    if 'percentage_of_analyzed_groups' in results[0].keys():
        data = {'filename': filename, 'contents': results, 'average': 'yes'}
    else:
        data = {'filename': filename, 'contents': results}
    return go_data, results


def get_file(request, filename):
    try:
        go_data, results = all_data(filename)
        # print(go_data[0],results)

        results = tab_file_to_dict(filename)
        if results != "No data found":
            test = 'percentage_of_analyzed_groups' in results[0].keys()
            result = merge(results, go_data)
            if 'percentage_of_analyzed_groups' in results[0].keys():
                data = {'filename': filename, 'contents': results, 'average': 'yes'}
            else:
                data = {'filename': filename, 'contents': results}
            # return JsonResponse(data)

            return render(request, 'tutorial/people.html', data)
        else:
            return render(request, 'tutorial/error.html')
    except:
        data = {'filename': 'No filename', 'contents': 'No data found'}
        return render(request, 'tutorial/people.html', data)
        # return JsonResponse({'Analysis Error': 'For given parameters no neighborhoods were analyzed'}, status=404)


def get_file_taxonomy(request, filename):
    try:
        results = tab_file_to_dict(filename)
        data = {'filename': filename, 'contents': results}
        # return JsonResponse(data)
        return render(request, 'tutorial/taxonomy.html', data)
    except FileNotFoundError:
        return JsonResponse({'error': 'File not found'}, status=404)


def parse_go_data(file_path):
    biological_process_data = []
    molecular_function_data = []
    cellular_component_data = []

    # read the data from the file
    with open(f"/usr/src/app/mediafiles/results/{file_path}.txt", "r") as file:
        lines = file.readlines()
        category = ""
        complete_data = {}
        for line in lines:
            if line.startswith("BIOLOGICAL PROCESS"):
                category = "biological_process"
            elif line.startswith("MOLECULAR FUNCTION"):
                category = "molecular_function"
            elif line.startswith("CELLULAR COMPONENT"):
                category = "cellular_component"
            elif line == "\n" or 'GO_id' in line:
                continue
            else:
                # split the line and store the data as a dictionary
                data = line.strip().split("\t")
                entry = {"GO_id": data[1], "term": data[2], "Neighborhood_domain_Pfam_ID": data[3]}
                # append the dictionary to the appropriate list
                if category == "biological_process":
                    biological_process_data.append(entry)
                elif category == "molecular_function":
                    molecular_function_data.append(entry)
                elif category == "cellular_component":
                    cellular_component_data.append(entry)

    # create dictionaries with GO_id as the key
    biological_process_dict = {entry["GO_id"]: entry for entry in biological_process_data}
    molecular_function_dict = {entry["GO_id"]: entry for entry in molecular_function_data}
    cellular_component_dict = {entry["GO_id"]: entry for entry in cellular_component_data}
    # complete_data = {biological_process_dict,molecular_function_dict,cellular_component_dict}
    # print(biological_process_data)
    return biological_process_dict, molecular_function_dict, cellular_component_dict


def get_file_ontology(request, filename):
    try:
        biological_process_dict, molecular_function_dict, cellular_component_dict = parse_go_data(filename)
        data = {'filename': filename,
                'biological_process_dict': biological_process_dict,
                'molecular_function_dict': molecular_function_dict,
                'cellular_component_dict': cellular_component_dict}
        # return JsonResponse(data)
        return render(request, 'tutorial/ontology.html', data)
    except FileNotFoundError:
        return JsonResponse({'error': 'File not found'}, status=404)


def merge(result, go):
    all_data = []
    for i in result:
        go_ids = []
        terms = []
        for k, v in go[0].items():
            if i['Neighborhood_domain_Pfam_ID'] in v['Neighborhood_domain_Pfam_ID']:
                go_ids.append(v['GO_id'])
                for j in list(set(v['term'].split(';'))):
                    terms.append(j)
        # i.update({'biological_process_GO_id':v['GO_id'],'biological_process_GO_term':v['term']})
        i.update({'biological_process_GO_id': ';'.join(go_ids), 'biological_process_GO_term': ';'.join(terms)})
        all_data.append(i)

    for i in result:
        go_ids = []
        terms = []
        for k, v in go[1].items():
            if i['Neighborhood_domain_Pfam_ID'] in v['Neighborhood_domain_Pfam_ID']:
                go_ids.append(v['GO_id'])
                for j in list(set(v['term'].split(';'))):
                    terms.append(j)
        i.update({'molecular_function_GO_id': ';'.join(go_ids), 'molecular_function_GO_term': ';'.join(terms)})
        all_data.append(i)

    for i in result:
        go_ids = []
        terms = []
        for k, v in go[2].items():
            if i['Neighborhood_domain_Pfam_ID'] in v['Neighborhood_domain_Pfam_ID']:
                go_ids.append(v['GO_id'])
                for j in list(set(v['term'].split(';'))):
                    terms.append(j)
        i.update({'cellular_component_GO_id': ';'.join(go_ids), 'cellular_component_GO_term': ';'.join(terms)})
        all_data.append(i)

    return (all_data)


class GetNeighSeqForAnalysis(APIView):
    """
    Get NeighSeq for analysis
    Method: GET
    """

    def get(self, request):
        neigh_seq = NeighAnalyzSequenceDatabase.objects.filter(status='Queue').order_by('id').first()
        if not neigh_seq:
            return Response(status=status.HTTP_404_NOT_FOUND)
        NeighAnalyzSequenceDatabase.objects.filter(id=neigh_seq.id).update(status='In progress')
        context = {
            'sequence_title': neigh_seq.sequence_title,
            'sequence': neigh_seq.sequence,
            'blast_cutoff': neigh_seq.blast_cutoff,
            'neigh_size': neigh_seq.neigh_size,
            'cut_off': neigh_seq.cut_off,
            'strand': neigh_seq.strand,
            'test_correction': neigh_seq.test_correction,
            'analysis_name': neigh_seq.analysis_name,
            'ontology_results': neigh_seq.ontology_results,
            'output_results': neigh_seq.output_results,
            'taxonomy_results': neigh_seq.output_results.replace('.txt', '_taxonomy.txt'),
        }
        return Response(context, status=status.HTTP_200_OK)


class UploadNeighSeqResults(APIView):
    """
    Upload NeighSeq results to S3 for given protein_id
    Method: POST
    """

    validator = QueryParamsValidator(["analysis_id"])

    def post(self, request):
        fileobjects = request.FILES.getlist("fileobjects")
        validated_params = self.validator.validate(request)

        analysis = NeighAnalyzSequenceDatabase.objects.filter(id=validated_params['analysis_id']).first()

        if not analysis:
            return Response(status=status.HTTP_404_NOT_FOUND)
        NeighAnalyzSequenceDatabase.objects.filter(id=validated_params['analysis_id']).update(status='Done')

        if not isinstance(validated_params, dict):
            return Response(
                f"Missing {validated_params}", status=status.HTTP_400_BAD_REQUEST
            )
        if not fileobjects:
            return Response("Missing files", status=status.HTTP_400_BAD_REQUEST)
        for file in fileobjects:
            with open(f"/app/mediafiles/results/{file.name}", "wb") as f:
                f.write(file.read())



        return Response(status=status.HTTP_200_OK)


class UpdateNeighSeqStatus(APIView):
    """
    Update NeighSeq status
    Method: POST
    """

    def post(self, request):
        data = request.data
        NeighAnalyzSequenceDatabase.objects.filter(id=data['id']).update(status=data['status'])
        return Response(status=status.HTTP_200_OK)


class GetNeighForAnalysis(APIView):
    """
    Get neighbourhood for analysis
    METHOD GET
    """

    def get(self, request):
        analysis = CompleteQueue.objects.filter(status='Queue').order_by('id').first()
        if not analysis:
            return Response(status=status.HTTP_404_NOT_FOUND)
        CompleteQueue.objects.filter(id=analysis.id).update(status='In progress')
        context = {
            "id": analysis.id,
            "tool": analysis.tool,
            "script": analysis.script,

        }
        return Response(context, status=status.HTTP_200_OK)


class UploadNeighResults(APIView):
    """
    Upload NeighSeq results to S3 for given protein_id
    Method: POST
    """

    validator = QueryParamsValidator(["analysis_id",])

    def post(self, request):
        fileobjects = request.FILES.getlist("fileobjects")
        validated_params = self.validator.validate(request)
        try:
            CompleteQueue.objects.filter(id=validated_params['analysis_id']).update(status='Done')
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        if not isinstance(validated_params, dict):
            return Response(
                f"Missing {validated_params}", status=status.HTTP_400_BAD_REQUEST
            )
        if not fileobjects:
            return Response("Missing files", status=status.HTTP_400_BAD_REQUEST)
        for file in fileobjects:
            with open(f"/app/mediafiles/results/{file.name}", "wb") as f:
                f.write(file.read())



        return Response(status=status.HTTP_200_OK)


class UpdateNeighStatus(APIView):
    """
    Update NeighSeq status
    Method: POST
    """

    def post(self, request):
        data = request.data
        CompleteQueue.objects.filter(id=data['id']).update(status=data['status'])
        return Response(status=status.HTTP_200_OK)
