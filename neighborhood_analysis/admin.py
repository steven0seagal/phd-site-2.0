from django.contrib import admin

# Register your models here.
from .models import NeighAnalyzDatabase, NeighAnalyzSequenceDatabase

class NeighAnalyzDatabaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'input', 'neigh_size')
    list_display_links = ('id','user_id','input' )
    search_fields = ('user_id','input')
    list_per_page = 25
admin.site.register(NeighAnalyzDatabase, NeighAnalyzDatabaseAdmin)

class NeighAnalyzSequenceDatabaseAdmin(admin.ModelAdmin):
    list_display = ('id','user_id', 'analysis_name')
    list_display_links = ('id','user_id', 'analysis_name')
    list_per_page = 25
admin.site.register(NeighAnalyzSequenceDatabase,NeighAnalyzSequenceDatabaseAdmin)
